/*
** EPITECH PROJECT, 2017
** my_revstr
** File description:
** Reverses a string
*/

#include <stdio.h>
#include <stdlib.h>

char	*my_revstr(char *str)
{
	int length = 0;
	int argc = 0;
	char charhold;

	if (str[0] == '\0')
		return ("");
	while (str[length + 1] != '\0')
		length = length + 1;
	while (length / 2 >= argc) {
		charhold = str[argc];
		str[argc] = str[length - argc];
		str[length - argc] = charhold;
		argc = argc + 1;
	}
	return (str);
}
