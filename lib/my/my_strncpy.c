/*
** EPITECH PROJECT, 2017
** my_strncpy
** File description:
** Copies n char in another string
*/

char	*my_strncpy(char *dest, char const *src, int n)
{
	int argc = 0;
	int length = 0;

	while (src[length] != '\0')
		length = length + 1;
	if (n > length) {
		while (argc <= length) {
			dest[argc] = src[argc];
			argc = argc + 1;
		}
	} else {
		while (argc < n) {
			dest[argc] = src[argc];
			argc = argc + 1;
		}
	}
	dest[argc] = '\0';
	return (dest);
}
