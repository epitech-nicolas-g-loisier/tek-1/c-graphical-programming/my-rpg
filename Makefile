
##
## EPITECH PROJECT, 2017
## Makefile
## File description:
## Compiles program
##

SRC	=	$(SRC_INIT_DIR)/main.c			\
		$(SRC_INIT_DIR)/create_donjon.c		\
		$(SRC_DISP_DIR)/display_window.c		\
		$(SRC_ALGO_DIR)/get_next_line.c		\
		$(SRC_INIT_DIR)/create_donjon_room.c		\
		$(SRC_INIT_DIR)/create_donjon_room_solo.c	\
		$(SRC_DISP_DIR)/display_minimap.c		\
		$(SRC_ALGO_DIR)/find_boss_room.c		\
		$(SRC_ALGO_DIR)/check_room.c			\
		$(SRC_ALGO_DIR)/find_shop_room.c		\
		$(SRC_ALGO_DIR)/check_big_square.c		\
		$(SRC_DISP_DIR)/display_xp.c			\
		$(SRC_DISP_DIR)/display_health.c		\
		$(SRC_INIT_DIR)/free_donjon.c		\
		$(SRC_DISP_DIR)/draw_background.c		\
		$(SRC_DISP_DIR)/draw_door.c			\
		$(SRC_INIT_DIR)/create_entity.c		\
		$(SRC_INIT_DIR)/destroy_entity.c		\
		$(SRC_ALGO_DIR)/check_collision.c		\
		$(SRC_ALGO_DIR)/check_player_collision.c	\
		$(SRC_DISP_DIR)/draw_entities.c		\
		$(SRC_DISP_DIR)/player_attack.c		\
		$(SRC_ALGO_DIR)/attack_zone.c		\
		$(SRC_ALGO_DIR)/player_collision.c		\
		$(SRC_DISP_DIR)/player_animation.c		\
		$(SRC_ALGO_DIR)/manage_keyboard.c		\
		$(SRC_DISP_DIR)/manage_player.c		\
		$(SRC_INIT_DIR)/read_file.c			\
		$(SRC_INIT_DIR)/load_monster.c		\
		$(SRC_ALGO_DIR)/update_monster.c		\
		$(SRC_ALGO_DIR)/move_monster.c		\
		$(SRC_ALGO_DIR)/monster_attack.c	\
		$(SRC_DISP_DIR)/monster_animation.c		\
		$(SRC_ALGO_DIR)/monster_stat.c		\
		$(SRC_ALGO_DIR)/monster_collision.c		\
		$(SRC_ALGO_DIR)/anim_room_move.c		\
		$(SRC_ALGO_DIR)/anim_room_utils.c	\
		$(SRC_ALGO_DIR)/check_room_dir.c	\
		$(SRC_ALGO_DIR)/button_states.c		\
		$(SRC_INIT_DIR)/create_scenes.c		\
		$(SRC_DISP_DIR)/display_scene.c		\
		$(SRC_DISP_DIR)/display_game_scene.c	\
		$(SRC_DISP_DIR)/end_game_animation.c	\
		$(SRC_INIT_DIR)/create_inventory.c	\
		$(SRC_ALGO_DIR)/button_action.c		\
		$(SRC_INIT_DIR)/create_settings.c	\
		$(SRC_DISP_DIR)/display_more_scene.c	\
		$(SRC_INIT_DIR)/create_how_to_play.c	\
		$(SRC_DISP_DIR)/display_pegi.c	\
		$(SRC_DISP_DIR)/display_title_screen.c	\
		$(SRC_DISP_DIR)/play_music.c	\
		$(SRC_INIT_DIR)/create_button.c	\
		$(SRC_DISP_DIR)/display_text.c	\
		$(SRC_DISP_DIR)/display_begin.c	\
		$(SRC_INIT_DIR)/create_level_up.c	\
		$(SRC_ALGO_DIR)/set_settings.c

SRC_DIR	=	$(realpath ./src)

SRC_INIT_DIR	=	$(realpath ./src/initialisation)

SRC_ALGO_DIR	=	$(realpath ./src/algorithm)

SRC_DISP_DIR	=	$(realpath ./src/display)

DEST_A	=	$(realpath ./lib/my)

NAME	=	my_rpg

OBJ	=	$(SRC:.c=.o)

CC	=	gcc

CFLAGS	=	-Wall -Wextra	\
		-Iinclude

LFLAGS	=	-L$(DEST_A) -lmy -lc_graph_prog

$(NAME):	$(OBJ)
	make -C $(DEST_A)
	gcc -o $(NAME) $(OBJ) $(LFLAGS)

all:	$(NAME)

clean:
	rm -f $(OBJ)
	make -C $(DEST_A) clean

fclean:
	rm -f $(NAME) $(OBJ)
	make -C $(DEST_A) fclean

re:	fclean all

.PHONY: all clean fclean re
