/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** header for entity
*/

#include <SFML/Graphics.h>
#include <SFML/Audio.h>

#ifndef ENTITY_H_
#define ENTITY_H_

enum entity_type {
	PLAYER,
	MOB,
	OBS,
	PROJ,
	RUPEE,
	MAX_TYPE_ID
};

typedef struct stat_s
{
	int health;
	int max_health;
	int xp;
	int max_xp;
	int lvl;
	int attack;
	int defense;
	int mob_killed;
	sfClock *high_frames;
} stat_t;

typedef struct texture_list {
	sfTexture *img;
	sfIntRect rect;
	sfVector2f size;
	struct texture_list *next;
} texture_list_t;

typedef struct entity {
	char type;
	char id;
	stat_t stat;
	sfRectangleShape *rect;
	texture_list_t texture;
	struct entity *next;
	sfClock *clock;
	int status;
	int dir;
} entity_t;

/* Attack */
int	check_if_hit(entity_t *, sfIntRect, sfIntRect);
void	attack_zone(entity_t *, entity_t *);
void	set_player_idle(entity_t *);

/* Update Monster */
void	update_monsters(sfRenderWindow *, entity_t *, entity_t *);
void	launch_moving_animation(entity_t *, int);
int	call_monster(sfRenderWindow *, entity_t *);
void	launch_monster_attack(entity_t *, sfIntRect *, int, int);

/* Monster moving / collision */
int	move_monster_x(entity_t *, sfVector2f, sfVector2f, float *);
int	move_monster_y(entity_t *, sfVector2f, sfVector2f, float *);
int	detect_player(entity_t *, entity_t *);
void	check_monster_collision(entity_t *, entity_t *);
void	define_monster_direction(entity_t *, sfVector2f, sfVector2f);

/* Monster statistic */
void	define_monster_stat(entity_t *);

/* Create entity */
entity_t	*create_entity(char, sfVector2f, texture_list_t *, sfVector2f);

/* Player colission */
int	apply_player_collision(entity_t *, entity_t *);

#endif /* ENTITY_H_ */
