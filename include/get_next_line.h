/*
** EPITECH PROJECT, 2018
** get_next_line.h
** File description:
** Defines the READ_SIZE macro
*/

#ifndef READ_SIZE
#	define READ_SIZE 64
#endif

#ifndef GNL_
#	define GNL_

char	*get_next_line(int fd);

#endif
