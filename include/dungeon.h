/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** header for dungeon
*/

#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "entity.h"
#include "scene.h"

#ifndef DUNGEON_H_
#define DUNGEON_H_

#ifndef TEXTURE_ICON
#define TEXTURE_ICON "assets/Map_icons.png"
#endif /* TEXTURE_ICON */

enum room_id {
	VOID,
	BLOCK,
	ROOM,
	MAIN,
	SHOP,
	MINI_BOSS,
	BOSS,
	KEY
};

typedef struct room {
	char **contain;
	entity_t *entity;
	bool complete;
	bool close;
	char id;
} room_t;

typedef struct donjon {
	room_t ***rooms;
	sfVector2i pos;
	int floor;
} donjon_t;

/* prototypes for donjon creation*/
donjon_t *init_donjon(int);
room_t	***create_up_room_solo(room_t ***, sfVector2i, int);
room_t	***create_right_room_solo(room_t ***, sfVector2i, int);
room_t	***create_down_room_solo(room_t ***, sfVector2i, int);
room_t	***create_left_room_solo(room_t ***, sfVector2i, int);
room_t	***create_up_room(room_t ***, sfVector2i, int, int);
room_t	***create_right_room(room_t ***, sfVector2i, int, int);
room_t	***create_down_room(room_t ***, sfVector2i, int, int);
room_t	***create_left_room(room_t ***, sfVector2i, int, int);
int	check_big_square(room_t ***, sfVector2i);
int	count_arround_room(room_t ***, sfVector2i);
entity_t	*create_entity(char, sfVector2f, texture_list_t *, sfVector2f);
void	draw_background(sfRenderWindow *, donjon_t *, sfVector2f);
int	check_collision(entity_t *);
void	check_player_collision(entity_t *, sfFloatRect *);
void	draw_entities(sfRenderWindow *, entity_t *);
int	apply_player_collision(entity_t *, entity_t *);
void	destroy_entity(entity_t *);
void	destroy_entity_list(entity_t *);

/* prototypes for room initialisation */
void	find_shop_room(room_t ***, int);
void	find_boss_room(room_t ***, int);
int	find_mini_boss_room(room_t ***, int);

void	free_donjon(donjon_t*);

/* Keyboard */
void	manage_keyboard(entity_t *, int *);

/* Player */
void	manage_player(entity_t *);
void	player_attack(entity_t *, entity_t *);
int	check_if_hit(entity_t *, sfIntRect, sfIntRect);
void	player_walk(entity_t *);

/* Door */
sfTexture	*door_texture(int);
char	draw_up_door(sfRenderWindow *, room_t ***, sfVector2i, sfVector2f);
char	draw_right_door(sfRenderWindow *, room_t ***, sfVector2i, sfVector2f);
char	draw_down_door(sfRenderWindow *, room_t ***, sfVector2i, sfVector2f);
char	draw_left_door(sfRenderWindow *, room_t ***, sfVector2i, sfVector2f);

/* anim move room */
void	move_room(sfRenderWindow *, donjon_t *, entity_t *);
int	anim_room_clock(int);
void    move_room_draw(sfRenderWindow*, sfVector2f*, donjon_t*, donjon_t*);
int     get_room_dir(sfRenderWindow *, entity_t *, donjon_t*);

/* Check room dir */
sfBool	check_if_down_room(room_t ***, sfVector2i);
sfBool	check_if_right_room(room_t ***, sfVector2i);
sfBool	check_if_up_room(room_t ***, sfVector2i);
sfBool	check_if_left_room(room_t ***, sfVector2i);
sfBool	check_if_room(room_t ***, sfVector2i, int);

#endif /* DUNGEON_H_ */
