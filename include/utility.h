/*
** EPITECH PROJECT, 2018
** Utility.h
** File description:
** usefull macro
*/

#ifndef ABS
#define ABS(x) (x >= 0 ? x : x * -1)
#endif /* ABS */
