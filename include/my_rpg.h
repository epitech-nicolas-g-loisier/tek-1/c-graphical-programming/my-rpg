/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Main header
*/

#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <stdbool.h>
#include "scene.h"
#include "dungeon.h"

#ifndef MY_WORLD_H_
#define MY_WORLD_H_

#define WIDTH 1920
#define HEIGHT 1080

#define X_MID	WIDTH / 2
#define Y_MID	HEIGHT / 2

typedef struct rpg {
	int index_scene;
	scene_t **scenes;
} rpg_t;

int	open_window(void);
void	analyse_event(sfRenderWindow *, sfEvent);

int	display_health(sfRenderWindow *, int, int);
int	display_xp(sfRenderWindow *, int, int);

//void	draw_door(sfRenderWindow *, room_t ***, sfVector2i);
void	draw_minimap(donjon_t *, sfRenderWindow *);
int	destroy_object(sfTexture*, sfRectangleShape*);

/* Scenes */
scene_t	**create_scenes(void);
void	display_scene(sfRenderWindow *, rpg_t *, donjon_t *, entity_t *);
void	display_game_scene(sfRenderWindow *, rpg_t *, donjon_t *, entity_t *);
void	launch_end_game(sfRenderWindow *);
void	open_scene(int *);
void	call_other_scene(sfRenderWindow *, int *, rpg_t *);
void	display_begin(sfRenderWindow *);
void	display_pegi(sfRenderWindow*);
void	play_music(int);
void	display_title_screen_anim(sfRenderWindow*);
void	display_title_screen(sfRenderWindow*, int);
void	display_text(sfRenderWindow*, sfVector2f, char*);
void	draw_level_up(sfRenderWindow *, int *, scene_t *);
void	set_vertical_sync(sfRenderWindow *, int);
sfTexture *get_button_texture(int);
#endif /* MY_WORLD_H_ */
