/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Scene prototype header
*/

#ifndef	SCENE_H
#define	SCENE_H

#include <SFML/Graphics.h>
#include <SFML/Audio.h>
#include <stdlib.h>

typedef struct	object {
	sfRectangleShape *rect;
	sfTexture *texture;
} object_t;

typedef	enum	button_state {
	IDLE,
	HOVER,
	CLICKED
} button_state_t;

typedef	struct	button {
	sfRectangleShape *rect;
	button_state_t state;
	void (*callback)();
	struct	button *next;
} button_t;

typedef	struct	scene {
	object_t *objects;
	button_t *buttons;
} scene_t;

/* Init scene */
scene_t	*init_menu(void);
scene_t	*init_pause_menu(void);
scene_t	*create_inventory(void);
scene_t	*create_settings(void);
scene_t	*create_how_to_play(void);
scene_t	*create_level_up(void);

/* Buttons update */
button_t	*create_button(sfVector2f, sfVector2f);
button_t	*create_menu_button(sfVector2f, sfVector2f);
button_t	*create_pause_button(sfVector2f, sfVector2f);
void	manage_button(button_t *, sfRenderWindow *, int *);
void	manage_button_window(button_t *, sfRenderWindow *);
void	set_button_status(sfRenderWindow *, int *, button_t *);
void    manage_settings_button(button_t *, sfRenderWindow *, int);

/* Button action */
void	play_button(int *);
void	resume_button(int *);
void	menu_button(int *);
void	quit_button(sfRenderWindow *);
void	settings_button(int *);
void	how_to_play_button(int *);
void	set_framerate(sfRenderWindow *, int);

#endif	/* SCENE_H */
