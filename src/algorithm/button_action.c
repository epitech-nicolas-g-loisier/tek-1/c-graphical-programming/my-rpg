/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Define button action
*/

#include "my_rpg.h"
#include "scene.h"

void	menu_button(int *index_scene)
{
	*index_scene = 1;
}

void	play_button(int *index_scene)
{
	*index_scene = 2;
}

void	resume_button(int *index_scene)
{
	*index_scene = 2;
}

void	settings_button(int *index_scene)
{
	*index_scene = 5;
}

void	quit_button(sfRenderWindow *window)
{
	sfRenderWindow_close(window);
}
