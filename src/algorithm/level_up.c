/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** calcul level up
*/

#include "my_rpg.h"

int	level_up(sfRenderWindow *window, entity_t *player, rpg_t *rpg)
{
	if (player->stat.xp < player->stat.max_xp)
		return (0);
	else {
		draw_level_up(window, index_scene, rpg->scenes[7]);
		player->stat.xp -= player->stat.max_xp;
		player->stat.level += 1;
		player->stat.max_xp = 500 * player->stat.level + 500;
		player->stat.health = player->stat.max_health;
	}
	return (0);
}
