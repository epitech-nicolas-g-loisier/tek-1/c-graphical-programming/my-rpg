/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** check if the room create a big room
*/

#include "dungeon.h"

static	int check_up_right(room_t ***donjon, sfVector2i pos)
{
	if (pos.y <= 0 || donjon[pos.y][pos.x + 1] == NULL) {
		return (0);
	}
	if (donjon[pos.y - 1][pos.x]->id == ROOM &&
		donjon[pos.y][pos.x + 1]->id == ROOM){
		if (donjon[pos.y - 1][pos.x + 1]->id == ROOM) {
			return (1);
		}
	}
	return (0);
}

static	int check_down_right(room_t ***donjon, sfVector2i pos)
{
	if (donjon[pos.y + 1] == NULL || donjon[pos.y][pos.x + 1] == NULL) {
		return (0);
	}
	if (donjon[pos.y][pos.x + 1]->id == ROOM &&
		donjon[pos.y + 1][pos.x]->id == ROOM){
		if (donjon[pos.y + 1][pos.x + 1]->id == ROOM) {
			return (1);
		}
	}
	return (0);
}

static	int check_down_left(room_t ***donjon, sfVector2i pos)
{
	if (donjon[pos.y + 1] == NULL || pos.x <= 0) {
		return (0);
	}
	if (donjon[pos.y + 1][pos.x]->id == ROOM &&
		donjon[pos.y][pos.x - 1]->id == ROOM){
		if (donjon[pos.y + 1][pos.x - 1]->id == ROOM) {
			return (1);
		}
	}
	return (0);
}

static	int check_up_left(room_t ***donjon, sfVector2i pos)
{
	if (pos.y <= 0 || pos.x <= 0) {
		return (0);
	}
	if (donjon[pos.y][pos.x - 1]->id == ROOM &&
		donjon[pos.y - 1][pos.x]->id == ROOM){
		if (donjon[pos.y - 1][pos.x - 1]->id == ROOM) {
			return (1);
		}
	}
	return (0);
}

int	check_big_square(room_t ***donjon, sfVector2i pos)
{
	int count = 0;

	count += check_up_right(donjon, pos);
	count += check_down_right(donjon, pos);
	count += check_down_left(donjon, pos);
	count += check_up_left(donjon, pos);
	return (count);
}
