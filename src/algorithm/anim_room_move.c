/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** do an aniamtion when we move the map
*/

#include "my_rpg.h"
#include "dungeon.h"

static	int move_up_room(sfRenderWindow *window, donjon_t *donjon)
{
	sfVector2f origin[2] = {{0, 0}, {0, 1230}};
	donjon_t tmp = *donjon;

	tmp.pos = (sfVector2i){donjon->pos.x, donjon->pos.y};
	if (check_if_up_room(donjon->rooms, donjon->pos) == sfFalse)
		return (0);
	donjon->pos.y -= 1;
	for (int move = 0; origin[1].y >= 0; move += 0) {
		move = anim_room_clock(1);
		if ((origin[1].y - move) <= 0)
			break;
		origin[0].y -= move;
		origin[1].y -= move;
		move_room_draw(window, origin, donjon, &tmp);
	}
	return (anim_room_clock(-1));
}

static	int move_right_room(sfRenderWindow *window, donjon_t *donjon)
{
	sfVector2f origin[2] = {{0, 0}, {-2070, 0}};
	donjon_t tmp = *donjon;

	tmp.pos = (sfVector2i){donjon->pos.x, donjon->pos.y};
	if (check_if_right_room(donjon->rooms, donjon->pos) == sfFalse)
		return (0);
	donjon->pos.x += 1;
	for (int move = 0; origin[1].x <= 0; move += 0) {
		move = anim_room_clock(0);
		if ((origin[1].x + move) >= 0)
			break;
		origin[0].x += move;
		origin[1].x += move;
		move_room_draw(window, origin, donjon, &tmp);
	}
	return (anim_room_clock(-1));
}

static	int move_down_room(sfRenderWindow *window, donjon_t *donjon)
{
	sfVector2f origin[2] = {{0, 0}, {0, -1230}};
	donjon_t tmp = *donjon;

	tmp.pos = (sfVector2i){donjon->pos.x, donjon->pos.y};
	if (check_if_down_room(donjon->rooms, donjon->pos) == sfFalse)
		return (0);
	donjon->pos.y += 1;
	for (int move = 0; origin[1].y <= 0; move += 0) {
		move = anim_room_clock(1);
		if ((origin[1].y + move) >= 0)
			break;
		origin[0].y += move;
		origin[1].y += move;
		move_room_draw(window, origin, donjon, &tmp);
	}
	return (anim_room_clock(-1));
}

static	int move_left_room(sfRenderWindow *window, donjon_t *donjon)
{
	sfVector2f origin[2] = {{0, 0}, {2070, 0}};
	donjon_t tmp = *donjon;

	tmp.pos = (sfVector2i){donjon->pos.x, donjon->pos.y};
	if (check_if_left_room(donjon->rooms, donjon->pos) == sfFalse)
		return (0);
	donjon->pos.x -= 1;
	for (int move = 0; origin[1].x >= 0; move += 0) {
		move = anim_room_clock(0);
		if ((origin[1].x - move) <= 0)
			break;
		origin[0].x -= move;
		origin[1].x -= move;
		move_room_draw(window, origin, donjon, &tmp);
	}
	return (anim_room_clock(-1));
}

void	move_room(sfRenderWindow *window, donjon_t *donjon, entity_t *player)
{
	int (*function[4])(sfRenderWindow *, donjon_t *);
	int dir = get_room_dir(window, player, donjon);

	function[0] = *move_down_room;
	function[1] = *move_right_room;
	function[2] = *move_up_room;
	function[3] = *move_left_room;
	if (dir >= 0 && dir < 4)
		function[dir](window, donjon);
}
