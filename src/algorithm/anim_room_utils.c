/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** clock for room animation
*/

#include "entity.h"
#include "my_rpg.h"

static int	door_move_player(entity_t *player, int idx, sfVector2u win_size,
				donjon_t *donjon)
{
	int modif_pos[4] = {200, 200, win_size.y - 200, win_size.x - 200};
	sfVector2f pos = sfRectangleShape_getPosition(player->rect);
	sfVector2f size = sfRectangleShape_getSize(player->rect);
	sfVector2f scale = sfRectangleShape_getScale(player->rect);

	if (check_if_room(donjon->rooms, donjon->pos, idx) == sfFalse)
		return (0);
	modif_pos[2] -= size.y * scale.y;
	modif_pos[3] += size.x * scale.x;
	if (idx == 0 || idx == 2)
		pos.y = modif_pos[idx];
	else
		pos.x = modif_pos[idx];
	sfRectangleShape_setPosition(player->rect, pos);
	return (0);
}

int	get_room_dir(sfRenderWindow *window, entity_t *player,
				donjon_t *donjon)
{
	sfVector2u size = sfRenderWindow_getSize(window);
	sfFloatRect door_pos[4] = {{size.x / 2 - 25, size.y - 170, 50, 20},
				{size.x - 170, size.y / 2 - 25, 20, 50},
				{size.x / 2 - 25, 150, 50, 20},
				{150, size.y / 2 - 25, 20, 50}};
	sfFloatRect hitbox = sfRectangleShape_getGlobalBounds(player->rect);
	sfBool check = sfFalse;

	for (int idx = 0; idx < 4; idx++) {
		check = sfFloatRect_intersects(&hitbox, &door_pos[idx], NULL);
		if (check == sfTrue && player->dir == idx) {
			door_move_player(player, idx, size, donjon);
			return (idx);
		}
	}
	return (-1);
}

void	move_room_draw(sfRenderWindow *window, sfVector2f origin[2],
			donjon_t *donjon, donjon_t *tmp)
{
	sfRenderWindow_clear(window, sfBlack);
	draw_background(window, tmp, origin[0]);
	draw_background(window, donjon, origin[1]);
	sfRenderWindow_display(window);
}

sfTexture *door_texture(int choice)
{
	static sfTexture *texture = NULL;

	if (texture == NULL)
		texture = sfTexture_createFromFile("assets/doors.png", NULL);
	if (choice == 1) {
		sfTexture_destroy(texture);
		texture = NULL;
	}
	return (texture);
}

int	anim_room_clock(int choice)
{
	static sfClock *clock = NULL;
	static sfTime time = {0};
	sfTime tmp_time = {0};
	double value[2] = {2.56, 1.44};
	int tmp = 0;

	if (choice == -1) {
		sfClock_destroy(clock);
		clock = NULL;
		time.microseconds = 0;
		return (0);
	} else if (clock == NULL)
		clock = sfClock_create();
	tmp_time = sfClock_getElapsedTime(clock);
	tmp = tmp_time.microseconds - time.microseconds;
	time = tmp_time;
	tmp = tmp / 1000;
	tmp = tmp * value[choice];
	return (tmp);
}
