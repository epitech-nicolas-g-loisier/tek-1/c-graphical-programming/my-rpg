/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Handle monster collision
*/

#include "my_rpg.h"
#include "entity.h"

void	avoid_obstacle(entity_t *entity, sfVector2f *old_pos, int axes)
{
	sfVector2f obs_pos = sfRectangleShape_getPosition(entity->rect);

	if (axes == 1) {
		if (obs_pos.x > old_pos->x)
			old_pos->y += 3;
		else
			old_pos->y -= 3;
	} else {
		if (obs_pos.y > old_pos->y)
			old_pos->x -= 3;
		else
			old_pos->x += 3;
	}
}

void	reset_pos(entity_t *monster, entity_t *entity, sfVector2f old_pos,
	int axes)
{
	if (entity->type == OBS || entity->type == MOB) {
		avoid_obstacle(entity, &old_pos, axes);
		sfRectangleShape_setPosition(monster->rect, old_pos);
	}
	else if (entity->type == PLAYER) {
		sfRectangleShape_setPosition(monster->rect, old_pos);
		monster->stat.mob_killed = 1;
	}
}

void	check_monster_x_collision(entity_t *entity_list, entity_t *monster)
{
	int inc = 0;
	entity_t *entity = entity_list;
	sfFloatRect hitbox[2] = {{0, 0, 0, 0}, {0, 0, 0, 0}};
	sfVector2f old_pos = sfRectangleShape_getPosition(monster->rect);
	sfVector2f current_pos = old_pos;
	sfVector2f player_pos = sfRectangleShape_getPosition(
	entity_list->rect);

	if (move_monster_x(monster, old_pos, player_pos, &current_pos.x))
		sfRectangleShape_setPosition(monster->rect, current_pos);
	hitbox[0] = sfRectangleShape_getGlobalBounds(monster->rect);
	while (entity != NULL) {
		hitbox[1] = sfRectangleShape_getGlobalBounds(entity->rect);
		if (sfFloatRect_intersects(&hitbox[0], &hitbox[1], NULL)
		== sfTrue) {
			reset_pos(monster, entity, old_pos, 1);
		}
		entity = entity->next;
		inc++;
	}
}

void	check_monster_y_collision(entity_t *entity_list, entity_t *monster)
{
	int inc = 0;
	entity_t *entity = entity_list;
	sfFloatRect hitbox[2] = {{0, 0, 0, 0}, {0, 0, 0, 0}};
	sfVector2f old_pos = sfRectangleShape_getPosition(monster->rect);
	sfVector2f current_pos = old_pos;
	sfVector2f player_pos = sfRectangleShape_getPosition(
	entity_list->rect);

	if (move_monster_y(monster, old_pos, player_pos, &current_pos.y))
		sfRectangleShape_setPosition(monster->rect, current_pos);
	hitbox[0] = sfRectangleShape_getGlobalBounds(monster->rect);
	while (entity != NULL) {
		hitbox[1] = sfRectangleShape_getGlobalBounds(entity->rect);
		if (sfFloatRect_intersects(&hitbox[0], &hitbox[1], NULL)
		== sfTrue) {
			reset_pos(monster, entity, old_pos, 0);
		}
		entity = entity->next;
		inc++;
	}
}

void	check_monster_collision(entity_t *entity_list, entity_t *monster)
{
	sfVector2f pos = {0, 0};
	sfVector2f old_pos = sfRectangleShape_getPosition(monster->rect);

	check_monster_x_collision(entity_list, monster);
	check_monster_y_collision(entity_list, monster);
	pos = sfRectangleShape_getPosition(monster->rect);
	define_monster_direction(monster, old_pos, pos);
}
