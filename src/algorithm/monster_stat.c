/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Handle monsters statistic
*/

#include "entity.h"

void	define_monster_stat(entity_t *monster)
{
	monster->stat.health = 20;
	monster->stat.attack = 2;
	monster->stat.defense = 0.8;
}
