/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Handle monster moving
*/

#include "my_rpg.h"
#include "entity.h"

int	move_monster_x(entity_t *monster, sfVector2f monster_pos,
			sfVector2f player_pos, float *pos)
{
	int speed = 3;
	sfFloatRect rect = sfRectangleShape_getGlobalBounds(monster->rect);

	monster_pos.x > player_pos.x && monster->dir != 2 ?
	*pos += -speed : 0;
	monster_pos.x < player_pos.x && monster->dir != 3 ?
	*pos += speed : 0;
	if (*pos - rect.width / 2 > 150 &&
	*pos + rect.width / 2 < (WIDTH - 150)) {
		return (1);
	}
	return (0);
}

int	move_monster_y(entity_t *monster, sfVector2f monster_pos,
			sfVector2f player_pos, float *pos)
{
	int speed = 3;
	sfFloatRect rect = sfRectangleShape_getGlobalBounds(monster->rect);

	monster_pos.y < player_pos.y && monster->dir != 0 ?
	*pos += speed : 0;
	monster_pos.y > player_pos.y && monster->dir != 1 ?
	*pos += -speed : 0;
	if (*pos > 150 && *pos + rect.height < (HEIGHT - 150)) {
		return (1);
	}
	return (0);
}

void	define_vertical_direction(entity_t *monster, int val, int val2)
{
	if (val2 < 0 && val == 0)
		monster->dir = 0;
	else if (val2 > 0 && val == 0)
		monster->dir = 1;
}

void	define_monster_direction(entity_t *monster, sfVector2f old_pos,
				sfVector2f pos)
{
	int val = 0;
	int val2 = 0;

	val = pos.x - old_pos.x;
	val2 = pos.y - old_pos.y;
	if (val < 0 && val2 == 0)
		monster->dir = 3;
	else if (val > 0 && val2 == 0)
		monster->dir = 2;
	define_vertical_direction(monster, val, val2);
}

int	detect_player(entity_t *player, entity_t *monster)
{
	sfIntRect rect = sfRectangleShape_getTextureRect(monster->rect);
	sfVector2f monster_pos = sfRectangleShape_getPosition(monster->rect);
	sfVector2f player_pos = sfRectangleShape_getPosition(player->rect);
	sfFloatRect player_rect = sfRectangleShape_getGlobalBounds(
	player->rect);
	sfFloatRect monster_rect = sfRectangleShape_getGlobalBounds(
	monster->rect);
	int zone = 300;

	if (player_pos.y + player_rect.height >= monster_pos.y - zone &&
	player_pos.y <= monster_pos.y + monster_rect.height + zone &&
	player_pos.x + player_rect.width >= monster_pos.x - zone &&
	player_pos.x <= monster_pos.x + monster_rect.width + zone) {
		check_monster_collision(player, monster);
		return (1);
	} else {
		rect.left = 0;
		sfRectangleShape_setTextureRect(monster->rect, rect);
		return (0);
	}
}
