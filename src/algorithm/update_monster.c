/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Draw monsters shape
*/

#include "my_rpg.h"
#include "entity.h"

void	config_monster(sfRenderWindow *window, entity_t *monster,
			sfVector2f scale)
{
	sfVector2f pos = sfRectangleShape_getPosition(monster->rect);
	sfFloatRect rect = {0, 0, 0, 0};
	int max = 0;

	if (pos.x == 0 && pos.y == 0) {
		rect = sfRectangleShape_getGlobalBounds(monster->rect);
		max = (WIDTH - 150) - rect.width / 2;
		pos.x = rand() % (max - 150) + 150;
		max = (HEIGHT - 150) - rect.height;
		pos.y = rand() % (max - 150) + 150;
		sfRectangleShape_setPosition(monster->rect, pos);
	}
	sfRectangleShape_setScale(monster->rect, scale);
	sfRenderWindow_drawRectangleShape(window, monster->rect, NULL);
}

void	update_monsters(sfRenderWindow *window, entity_t *monster,
			entity_t *player)
{
	sfVector2f scale = {1, 1};
	entity_t *tmp = monster;
	int inc = 0;

	while (tmp != NULL) {
		scale = sfRectangleShape_getScale(tmp->rect);
		if (tmp->dir != 3 && scale.x < 0)
			scale.x *= -1;
		config_monster(window, tmp, scale);
		if (detect_player(player, tmp))
			launch_moving_animation(tmp, inc);
		tmp = tmp->next;
		inc++;
	}
}
