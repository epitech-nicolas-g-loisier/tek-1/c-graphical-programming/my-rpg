/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to apply collision
*/

#include "dungeon.h"
#include "entity.h"

static void	remove_entity_from_list(entity_t *entity_list,
				entity_t *projectile)
{
	while (entity_list->next != projectile)
		entity_list = entity_list->next;
	entity_list->next = entity_list->next->next;
}

static int	restart_clock(entity_t *player)
{
	if (player->stat.high_frames == NULL)
		player->stat.high_frames = sfClock_create();
	else
		sfClock_restart(player->stat.high_frames);
	if (player->stat.high_frames == NULL)
		return (84);
	return (0);
}

int	apply_player_collision(entity_t *player, entity_t *mob)
{
	int mob_attack_stat = mob->stat.attack;

	if (mob->type == PROJ) {
		remove_entity_from_list(player, mob);
		destroy_entity(mob);
	}
	if (player->stat.high_frames != NULL && sfClock_getElapsedTime
	(player->stat.high_frames).microseconds < 2000000) {
		return (1);
	}
	player->stat.health -= (mob_attack_stat <= player->stat.defense) ?
	1 : mob_attack_stat - player->stat.defense;
	return (restart_clock(player));
}