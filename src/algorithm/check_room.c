/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** check if are room arround the given room
*/

#include "dungeon.h"

static	int count_vert_room(room_t ***donjon, sfVector2i pos)
{
	int count = 0;

	if (donjon[pos.y + 1] == NULL) {
		count += 0;
	} else if (donjon[pos.y + 1][pos.x]->id != VOID &&
			donjon[pos.y + 1][pos.x]->id != BLOCK) {
		count++;
	}
	if (pos.y < 1) {
		count += 0;
	} else if (donjon[pos.y - 1][pos.x]->id != VOID &&
			donjon[pos.y - 1][pos.x]->id != BLOCK) {
		count++;
	}
	return (count);
}

static	int count_hori_room(room_t ***donjon, sfVector2i pos)
{
	int count = 0;

	if (donjon[pos.y][pos.x + 1] == NULL) {
		count += 0;
	} else if (donjon[pos.y][pos.x + 1]->id != VOID &&
			donjon[pos.y][pos.x + 1]->id != BLOCK) {
		count++;
	}
	if (pos.x < 1) {
		count += 0;
	} else if (donjon[pos.y][pos.x - 1]->id != VOID &&
			donjon[pos.y][pos.x - 1]->id != BLOCK) {
		count++;
	}
	return (count);
}

int	count_arround_room(room_t ***donjon, sfVector2i pos)
{
	int nb_room = 0;

	nb_room += count_vert_room(donjon, pos);
	nb_room += count_hori_room(donjon, pos);
	return (nb_room);
}
