/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to handle button
*/

#include "my_rpg.h"

static int	check_button_interaction(button_t *button, sfVector2i mouse_pos)
{
	sfFloatRect bounds = sfRectangleShape_getGlobalBounds(button->rect);

	if (sfMouse_isButtonPressed(sfMouseLeft) == sfTrue && mouse_pos.x <
	bounds.left + bounds.width && mouse_pos.x > bounds.left &&
	mouse_pos.y < bounds.top + bounds.height && mouse_pos.y > bounds.top) {
		return (2);
	}
	else if (mouse_pos.x < bounds.left + bounds.width && mouse_pos.x >
	bounds.left && mouse_pos.y < bounds.top + bounds.height &&
	mouse_pos.y > bounds.top) {
		return (1);
	}
	return (0);
}

void	manage_button_window(button_t *button, sfRenderWindow *window)
{
	sfVector2i mouse_pos = sfMouse_getPosition((sfWindow *) window);

	if (button->state != IDLE &&
		check_button_interaction(button, mouse_pos) == 0) {
		sfRectangleShape_setFillColor(button->rect, sfWhite);
		button->state = IDLE;
	}
	else if (button->state != HOVER &&
	check_button_interaction(button, mouse_pos) == 1) {
		sfRectangleShape_setFillColor(button->rect, sfGreen);
		button->state = HOVER;
	} else {
		if (button->state != CLICKED &&
		check_button_interaction(button, mouse_pos) == 2) {
			sfRectangleShape_setFillColor(button->rect, sfCyan);
			button->state = CLICKED;
			button->callback(window);
		}
	}
}

void	manage_button(button_t *button, sfRenderWindow *window,
			int *index_scene)
{
	sfVector2i mouse_pos = sfMouse_getPosition((sfWindow *) window);

	if (button->state != IDLE &&
	check_button_interaction(button, mouse_pos) == 0) {
		sfRectangleShape_setFillColor(button->rect, sfWhite);
		button->state = IDLE;
	}
	else if (button->state != HOVER &&
	check_button_interaction(button, mouse_pos) == 1) {
		sfRectangleShape_setFillColor(button->rect, sfGreen);
		button->state = HOVER;
	} else {
		if (button->state != CLICKED &&
		check_button_interaction(button, mouse_pos) == 2) {
			sfRectangleShape_setFillColor(button->rect, sfCyan);
			button->state = CLICKED;
			button->callback(index_scene);
		}
	}
}

void	manage_settings_button(button_t *button, sfRenderWindow *window,
			int button_id)
{
	sfVector2i mouse_pos = sfMouse_getPosition((sfWindow *) window);

	if (button->state != IDLE &&
	check_button_interaction(button, mouse_pos) == 0) {
		sfRectangleShape_setFillColor(button->rect, sfWhite);
		button->state = IDLE;
	}
	else if (button->state != HOVER &&
	check_button_interaction(button, mouse_pos) == 1) {
		sfRectangleShape_setFillColor(button->rect, sfGreen);
		button->state = HOVER;
	} else {
		if (button->state != CLICKED &&
		check_button_interaction(button, mouse_pos) == 2) {
			sfRectangleShape_setFillColor(button->rect, sfCyan);
			button->state = CLICKED;
			button->callback(window, button_id);
		}
	}
}
