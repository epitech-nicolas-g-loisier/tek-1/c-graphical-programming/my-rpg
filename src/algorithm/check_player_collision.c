/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** check collisions between the player and other entities
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "dungeon.h"
#include "utility.h"

static void	check_move_possible(sfKeyCode key1, sfKeyCode key2, int limit2,
				float *pos)
{
	int limit1 = (key1 == sfKeyZ ? 100 : 150);

	if (sfKeyboard_isKeyPressed(key1) == sfTrue && *pos > limit1)
		*pos = *pos - 10;
	if (sfKeyboard_isKeyPressed(key2) == sfTrue && *pos < limit2)
		*pos = *pos + 10;
}

static void	apply_collision(entity_t *player, entity_t *entity,
				sfVector2f oldpos)
{
	if (entity->type == OBS)
		sfRectangleShape_setPosition(player->rect, oldpos);
	if (entity->type == MOB || entity->type == PROJ)
		apply_player_collision(player, entity);
}

static void	check_vertical_collision(entity_t *entity_list,
				sfFloatRect *hitbox_list)
{
	entity_t *entity = entity_list->next;
	sfVector2f oldpos = sfRectangleShape_getPosition(entity_list->rect);
	sfVector2f size = sfRectangleShape_getSize(entity_list->rect);
	sfVector2f scale = sfRectangleShape_getScale(entity_list->rect);
	sfVector2f pos = oldpos;
	int limit = 920 - (size.y * ABS(scale.y));

	check_move_possible(sfKeyZ, sfKeyS, limit, &pos.y);
	sfRectangleShape_setPosition(entity_list->rect, pos);
	hitbox_list[0] = sfRectangleShape_getGlobalBounds(entity_list->rect);
	for (int i = 1; entity; i++) {
		if (sfFloatRect_intersects(&hitbox_list[i], &hitbox_list[0],
			NULL) == sfTrue)
			apply_collision(entity_list, entity, oldpos);
		entity = entity->next;
	}
}

static void	check_horizontal_collision(entity_t *entity_list,
				sfFloatRect *hitbox_list)
{
	entity_t *entity = entity_list->next;
	sfVector2f oldpos = sfRectangleShape_getPosition(entity_list->rect);
	sfVector2f size = sfRectangleShape_getSize(entity_list->rect);
	sfVector2f scale = sfRectangleShape_getScale(entity_list->rect);
	sfVector2f pos = oldpos;
	int limit = 1770 - (size.x * ABS(scale.x));

	check_move_possible(sfKeyQ, sfKeyD, limit, &pos.x);
	sfRectangleShape_setPosition(entity_list->rect, pos);
	hitbox_list[0] = sfRectangleShape_getGlobalBounds(entity_list->rect);
	for (int i = 1; entity; i++) {
		if (sfFloatRect_intersects(&hitbox_list[i], &hitbox_list[0],
			NULL) == sfTrue)
			apply_collision(entity_list, entity, oldpos);
		entity = entity->next;
	}
}

void	check_player_collision(entity_t *entity_list, sfFloatRect *hitbox_list)
{
	if (entity_list->status < 2) {
		check_horizontal_collision(entity_list, hitbox_list);
		check_vertical_collision(entity_list, hitbox_list);
	}
}