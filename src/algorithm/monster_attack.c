/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Handle monster attack
*/

#include "entity.h"

void	launch_monster_attack(entity_t *monster, sfIntRect *rect, int count,
				int id)
{
	int offset_rect[3][2] = {{380, 418}, {140, 256}, {195, 383}};
	int mob = 0;

	monster->stat.mob_killed == 1 && id == 1 ? mob = 2 :
	monster->stat.mob_killed == 0 && id == 1, mob = 1;
	if (rect->left >= offset_rect[mob][count]) {
		rect->left = 0;
		monster->stat.mob_killed = 0;
	}
}
