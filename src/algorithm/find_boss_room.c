/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** find the boss room
*/

#include "dungeon.h"
#include "utility.h"

static	int check_dist(int dist, sfVector2i room, sfVector2i idx,
			sfVector2i *pos)
{
	int tmp_dist = 0;
	sfVector2i diff;

	diff.x = room.x - idx.x;
	diff.y = room.y - idx.y;
	tmp_dist = ABS(diff.x) + ABS(diff.y);
	if (dist < tmp_dist) {
		pos->x = idx.x;
		pos->y = idx.y;
		return (tmp_dist);
	} else {
		return (dist);
	}
}

static	int delete_room(room_t ***donjon)
{
	int idx_y = 0;
	int idx_x = 0;

	while (donjon[idx_y] != NULL) {
		if (donjon[idx_y][idx_x]->id == ROOM) {
			donjon[idx_y][idx_x]->id = VOID;
			return (0);
		}
		idx_x++;
		if (donjon[idx_y][idx_x] == NULL) {
			idx_x = 0;
			idx_y++;
		}
	}
	return (0);
}

static	int little_loop(int dist, sfVector2i room[2], sfVector2i idx,
			room_t ***donjon)
{
	for (idx.x = 0; donjon[idx.y][idx.x] != NULL; idx.x++) {
		if (donjon[idx.y][idx.x]->id == ROOM &&
			count_arround_room(donjon, idx) <= 1) {
			dist = check_dist(dist, room[0], idx, &room[1]);
		}
	}
	return (dist);
}

void	find_boss_room(room_t ***donjon, int size)
{
	sfVector2i room[2] = {{(size / 2), (size / 2)}, {0, 0}};
	sfVector2i idx = {0, 0};
	int dist = -1;

	for (idx.y = 0; donjon[idx.y] != NULL; idx.y++) {
		dist = little_loop(dist, &room[0], idx, donjon);
	}
	if (dist > 0)
		donjon[room[1].y][room[1].x]->id = BOSS;
	else {
		delete_room(donjon);
		find_boss_room(donjon, size);
	}
}

int	find_mini_boss_room(room_t ***donjon, int size)
{
	sfVector2i room[2] = {{(size / 2), (size / 2)}, {0, 0}};
	sfVector2i idx = {0, 0};
	int dist = -1;

	if (!(rand() % 2))
		return (0);;
	for (idx.y = 0; donjon[idx.y] != NULL; idx.y++) {
		dist = little_loop(dist, &room[0], idx, donjon);
	}
	if (dist != 0)
		donjon[room[1].y][room[1].x]->id = MINI_BOSS;
	return (0);
}
