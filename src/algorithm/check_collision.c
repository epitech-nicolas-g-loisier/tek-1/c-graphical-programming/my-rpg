/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** check collisions between entities
*/

#include <stdlib.h>
#include "dungeon.h"

static void	correct_hitbox(sfFloatRect *hitbox, char entity_type)
{
	if (entity_type == OBS)
		hitbox->height *= 0.3;
}

int	check_collision(entity_t *entity_list)
{
	entity_t *entity = entity_list;
	int entity_count = 0;
	sfFloatRect *hitbox_list = NULL;

	while (entity) {
		entity_count++;
		entity = entity->next;
	}
	hitbox_list = malloc(sizeof(sfFloatRect) * (entity_count + 1));
	if (!hitbox_list)
		return (84);
	entity = entity_list;
	for (int i = 0; entity; i++) {
		hitbox_list[i] = sfRectangleShape_getGlobalBounds(entity->rect);
		correct_hitbox(&hitbox_list[i], entity->type);
		entity = entity->next;
	}
	check_player_collision(entity_list, hitbox_list);
	free(hitbox_list);
	return (0);
}