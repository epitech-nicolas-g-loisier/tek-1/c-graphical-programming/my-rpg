/*
** EPITECH PROJECT, 2018
** my_rpg
** File description:
** Handle settings
*/

#include "my_rpg.h"

void	set_framerate(sfRenderWindow *window, int button_nbr)
{
	if (button_nbr == 1) {
		sfRenderWindow_setFramerateLimit(window, 30);
	}
	else if (button_nbr == 2) {
		sfRenderWindow_setFramerateLimit(window, 60);
	}
}

void	set_vertical_sync(sfRenderWindow *window, int button_nbr)
{
	if (button_nbr == 3) {
		sfRenderWindow_setVerticalSyncEnabled(window, sfTrue);
	}
	else if (button_nbr == 4) {
		sfRenderWindow_setVerticalSyncEnabled(window, sfFalse);
	}
}
