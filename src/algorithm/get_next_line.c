/*
** EPITECH PROJECT, 2018
** get_next_line
** File description:
** Returns the next line from the feed given as arg
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include "get_next_line.h"

static char	*change_alloc(char *line, int counter, bool free_line)
{
	char *dest = NULL;

	if (free_line == true) {
		free(line);
		return (NULL);
	}
	dest = malloc(sizeof(char) * (READ_SIZE + counter + 1));
	if (dest == NULL) {
		free(line);
		return (NULL);
	}
	for (int i = 0; i < counter + READ_SIZE; i++) {
		if (i < counter)
			dest[i] = line[i];
		else
			dest[i] = '\0';
	}
	free(line);
	return (dest);
}

static void	keep_buffer(char buff[READ_SIZE + 1], bool copy_old_buffer)
{
	static char buffer[READ_SIZE + 1] = "\0";
	int i = 0;

	if (copy_old_buffer) {
		while (buffer[i] != '\0') {
			buff[i] = buffer[i];
			i++;
		}
		for (int j = i; j < READ_SIZE; j++)
			buff[j] = '\0';
	} else {
		while (buff[i] != '\0') {
			buffer[i] = buff[i];
			i++;
		}
		buffer[i] = '\0';
	}
}

static bool	store_line(char *line, int status)
{
	int i = 0;

	while (i < status) {
		if (line[i] == '\n') {
			keep_buffer(&line[i + 1], false);
			line[i] = '\0';
			break;
		}
		i++;
	}
	if (i == status)
		return (false);
	return (true);
}

static char	*check_buffer(int *len, int *status)
{
	char *line = NULL;
	char buffer[READ_SIZE + 1] = "";

	keep_buffer(&buffer[0], true);
	if (buffer[0] == '\0')
		return (NULL);
	while (buffer[*len] != '\n' && buffer[*len] != '\0')
		*len = *len + 1;
	if (buffer[*len] == '\n')
		*status = 0;
	line = malloc(sizeof(char) * (*len + 1));
	if (!line)
		return (NULL);
	for (int i = 0; i < *len; i++)
		line[i] = buffer[i];
	line[*len] = '\0';
	for (int i = *len + 1; i < READ_SIZE; i++)
		buffer[i - *len - 1] = buffer[i];
	keep_buffer(buffer, false);
	return (line);
}

char	*get_next_line(int fd)
{
	char *line = NULL;
	int pos = 0;
	int status = 1;

	line = check_buffer(&pos, &status);
	if (status == 0)
		return (line);
	while (status > 0) {
		line = change_alloc(line, pos, false);
		if (!line)
			return (NULL);
		status = read(fd, &line[pos], READ_SIZE);
		line[pos + status] = '\0';
		if (store_line(&line[pos], status))
			return (line);
		pos += status;
	}
	if (pos == 0)
		return (change_alloc(line, 0, true));
	return (line);
}
