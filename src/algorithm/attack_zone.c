/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to handle attack hits
*/

#include "dungeon.h"
#include "entity.h"

static sfFloatRect	get_hitzone_rect(entity_t *link)
{
	sfFloatRect hitzone = sfRectangleShape_getGlobalBounds(link->rect);

	if (link->dir == 0 || link->dir == 2) {
		hitzone.left += 6;
		hitzone.top += (link->dir == 0) ? 35 : -3;
		hitzone.width = 34;
		hitzone.height = 8;
	}
	else if (link->dir == 1 || link->dir == 3) {
		hitzone.left += (link->dir == 1) ? 35 : -3;
		hitzone.top += 6;
		hitzone.width = 8;
		hitzone.height = 34;
	}
	return (hitzone);
}

static int	check_intersects(sfFloatRect hitzone, sfFloatRect monster_rect)
{
	if (sfFloatRect_intersects(&hitzone, &monster_rect, NULL) == sfTrue) {
		return (1);
	}
	return (0);
}

static void	relink_entity(entity_t *link, entity_t *mob)
{
	while (link->next != mob) {
		link = link->next;
	}
	link->next = mob->next;
	destroy_entity(mob);
}

static void	attack_mob(entity_t *link, entity_t *mob)
{
	int damage = link->stat.attack - mob->stat.defense;

	if (damage < 1) {
		damage = 1;
	}
	mob->stat.health -= damage;
	if (mob->stat.health <= 0) {
		link->stat.xp += 100;
		relink_entity(link, mob);
	}
}

void	attack_zone(entity_t *link, entity_t *entity_list)
{
	sfFloatRect hitzone = get_hitzone_rect(link);

	while (entity_list != NULL) {
		if (entity_list->type == MOB && check_intersects(hitzone,
		sfRectangleShape_getGlobalBounds(entity_list->rect)) == 1) {
			attack_mob(link, entity_list);
		}
		entity_list = entity_list->next;
	}
}
