/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** check if the are room
*/

#include "my_rpg.h"

sfBool	check_if_down_room(room_t ***donjon, sfVector2i pos)
{
	pos.y += 1;
	if (donjon == NULL)
		return (sfFalse);
	else if (donjon[pos.y] == NULL)
		return (sfFalse);
	if (donjon[pos.y][pos.x]->id == VOID)
		return (sfFalse);
	else if (donjon[pos.y][pos.x]->id == BLOCK)
		return (sfFalse);
	else
		return (sfTrue);
}

sfBool	check_if_right_room(room_t ***donjon, sfVector2i pos)
{
	pos.x += 1;
	if (donjon == NULL)
		return (sfFalse);
	else if (donjon[pos.y][pos.x] == NULL)
		return (sfFalse);
	if (donjon[pos.y][pos.x]->id == VOID)
		return (sfFalse);
	else if (donjon[pos.y][pos.x]->id == BLOCK)
		return (sfFalse);
	else
		return (sfTrue);
}

sfBool	check_if_up_room(room_t ***donjon, sfVector2i pos)
{
	pos.y -= 1;
	if (donjon == NULL)
		return (sfFalse);
	else if (pos.y < 0)
		return (sfFalse);
	if (donjon[pos.y][pos.x]->id == VOID)
		return (sfFalse);
	else if (donjon[pos.y][pos.x]->id == BLOCK)
		return (sfFalse);
	else
		return (sfTrue);
}

sfBool	check_if_left_room(room_t ***donjon, sfVector2i pos)
{
	pos.x -= 1;
	if (donjon == NULL)
		return (sfFalse);
	else if (pos.x < 0)
		return (sfFalse);
	if (donjon[pos.y][pos.x]->id == VOID)
		return (sfFalse);
	else if (donjon[pos.y][pos.x]->id == BLOCK)
		return (sfFalse);
	else
		return (sfTrue);
}

sfBool	check_if_room(room_t ***donjon, sfVector2i pos, int idx)
{
	sfBool (*function[4])(room_t***, sfVector2i);
	sfBool ret = sfFalse;

	function[0] = *check_if_down_room;
	function[1] = *check_if_right_room;
	function[2] = *check_if_up_room;
	function[3] = *check_if_left_room;

	if (idx < 0 || idx > 3)
		return (ret);
	else if (donjon == NULL)
		return (ret);
	ret = function[idx](donjon, pos);
	return (ret);
}
