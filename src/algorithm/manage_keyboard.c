/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to analyse events
*/

#include "my_rpg.h"
#include "entity.h"

static int	change_scene(int *index_scene)
{
	static int changes[2][6] = {{0, 1, 2, 3, 4, 5}, {1, 1, 4, 2, 2, 5}};
	int index = 0;

	while (index != 6) {
		if (changes[0][index] == *index_scene) {
			*index_scene = changes[1][index];
			return (0);
		}
		index++;
	}
	return (0);
}

static int	manage_escape(int *index_scene)
{
	static int status = 0;

	if (sfKeyboard_isKeyPressed(sfKeyEscape) != sfTrue && status == 1) {
		status = 0;
		return (0);
	}
	else if (sfKeyboard_isKeyPressed(sfKeyEscape)
		== sfTrue && status != 1) {
		status = 1;
		return (change_scene(index_scene));
	}
	return (0);
}

static int	manage_move_keys(entity_t *link, sfKeyCode *move_keys)
{
	int index = 0;

	if (sfKeyboard_isKeyPressed(move_keys[link->dir]) == sfTrue) {
		return (1);
	}
	while (index != 4) {
		if (sfKeyboard_isKeyPressed(move_keys[index]) == sfTrue) {
			link->dir = index;
			return (1);
		}
		index++;
	}
	return (0);
}

static void	manage_attack_key(entity_t *link, sfKeyCode attack_key)
{
	if (sfKeyboard_isKeyPressed(attack_key) == sfTrue) {
		link->status = 2;
	}
}

void	manage_keyboard(entity_t *link, int *index_scene)
{
	sfKeyCode keys[4] = {sfKeyS, sfKeyD, sfKeyZ, sfKeyQ};
	sfKeyCode attack_key = sfKeyF;

	manage_escape(index_scene);
	if (*index_scene != 2) {
		return;
	}
	else if (sfKeyboard_isKeyPressed(sfKeyI) && *index_scene == 2) {
		*index_scene = 3;
	}
	manage_attack_key(link, attack_key);
	if (link->status != 2 && manage_move_keys(link, keys) == 1) {
		link->status = 1;
	}
	else if (link->status != 2) {
		link->status = 0;
	}
}
