/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to read file
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "get_next_line.h"

static char	**realloc_str_array(char **file, char *line, int size)
{
	char **new_file = malloc(sizeof(char *) * (size + 1));
	int index = 0;

	if (new_file == NULL) {
		return (NULL);
	}
	while (index != size - 1) {
		new_file[index] = file[index];
		index++;
	}
	new_file[index] = line;
	new_file[index + 1] = NULL;
	free(file);
	return (new_file);
}

char	**read_file(char *path)
{
	char **file = NULL;
	int fd = open(path, O_RDONLY);
	char *line = NULL;
	int size = 0;

	if (fd == -1) {
		return (NULL);
	}
	line = get_next_line(fd);
	while (line != NULL) {
		size++;
		file = realloc_str_array(file, line, size);
		if (file == NULL) {
			close(fd);
			return (NULL);
		}
		line = get_next_line(fd);
	}
	close(fd);
	return (file);
}
