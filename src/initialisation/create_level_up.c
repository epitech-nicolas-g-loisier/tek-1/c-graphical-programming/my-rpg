/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Initialize level_up scene
*/

#include "my_rpg.h"

scene_t	*create_level_up(void)
{
	scene_t *scene = malloc(sizeof(scene_t));

	scene->objects = malloc(sizeof(object_t));
	if (scene == NULL || scene->objects == NULL)
		return (NULL);
	scene->objects->rect = sfRectangleShape_create();
	sfRectangleShape_setSize(scene->objects->rect, (sfVector2f){300, 150});
	sfRectangleShape_setFillColor(scene->objects->rect, sfBlack);
	sfRectangleShape_setPosition(scene->objects->rect,
	(sfVector2f){810, 465});
	scene->buttons = create_button((sfVector2f){810, 515},
	(sfVector2f){100, 100});
	scene->buttons->callback = &resume_button;
	scene->buttons->next = create_button((sfVector2f){910, 515},
	(sfVector2f){100, 100});
	scene->buttons->next->callback = &resume_button;
	scene->buttons->next->next = create_button((sfVector2f){1010, 515},
	(sfVector2f){100, 100});
	scene->buttons->next->next->callback = &resume_button;
	return (scene);
}
