/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Initialize how to play scene
*/

#include "my_rpg.h"

scene_t	*create_how_to_play(void)
{
	scene_t *scene = malloc(sizeof(scene_t));

	if (scene == NULL)
		return (NULL);
	scene->buttons = create_menu_button((sfVector2f){1710, 1020},
	(sfVector2f){200, 50});
	scene->buttons->callback = &menu_button;
	return (scene);
}

void	how_to_play_button(int *index_scene)
{
	*index_scene = 6;
}
