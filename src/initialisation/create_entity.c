/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** create an entity
*/

#include "my_rpg.h"

static	void init_rect(sfRectangleShape *rect, texture_list_t *texture,
			sfVector2f scale, sfVector2f pos)
{
	sfVector2f size = {texture->rect.width, texture->rect.height};

	sfRectangleShape_setTexture(rect, texture->img, sfFalse);
	sfRectangleShape_setTextureRect(rect, texture->rect);
	sfRectangleShape_setSize(rect, size);
	sfRectangleShape_scale(rect, scale);
	sfRectangleShape_setPosition(rect, pos);
}

static	void create_player(entity_t *player, sfVector2f *pos)
{
	player->stat.max_health = 10;
	player->stat.health = 10;
	player->stat.max_xp = 1000;
	player->stat.xp = 0;
	pos->x = X_MID;
	pos->y = Y_MID;
}

static	entity_t *init_stat(entity_t *entity)
{
	entity->stat.health = 10;
	entity->stat.max_health = 10;
	entity->stat.xp = 0;
	entity->stat.max_xp = 0;
	entity->stat.lvl = 0;
	entity->stat.attack = 0;
	entity->stat.defense = 0;
	entity->stat.mob_killed = 0;
	entity->stat.high_frames = NULL;
	return (entity);
}

entity_t *create_entity(char type, sfVector2f pos, texture_list_t *texture,
			sfVector2f scale)
{
	entity_t *entity = malloc(sizeof(entity_t));

	if (entity == NULL)
		return (NULL);
	entity->type = type;
	entity->dir = 0;
	entity->next = NULL;
	entity->clock = sfClock_create();
	entity->status = 0;
	entity->rect = sfRectangleShape_create();
	entity = init_stat(entity);
	if (!entity->rect) {
		free(entity);
		return (NULL);
	} else if (type == PLAYER)
		create_player(entity, &pos);
	init_rect(entity->rect, texture, scale, pos);
	return (entity);
}
