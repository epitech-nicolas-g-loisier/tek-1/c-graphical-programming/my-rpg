/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** create the donjon's rooms
*/

#include "dungeon.h"

static	room_t	*init_room(void)
{
	room_t *room = malloc(sizeof(room_t));

	if (room == NULL)
		return (NULL);
	room->id = VOID;
	room->close = false;
	room->complete = false;
	return (room);
}

static room_t	***init_donjon_room(int size)
{
	room_t ***donjon = malloc(sizeof(room_t**) * (size + 1));
	sfVector2i idx = {0, 0};

	if (donjon == NULL)
		return (NULL);
	while (idx.y < size) {
		donjon[idx.y] = malloc(sizeof(room_t*) * (size + 1));
		if (donjon[idx.y] == NULL)
			return (NULL);
		for (idx.x = 0; idx.x < size; idx.x++) {
			donjon[idx.y][idx.x] = init_room();
		}
		donjon[idx.y][idx.x] = NULL;
		idx.y++;
	}
	donjon[idx.y] = NULL;
	return (donjon);
}

static void	*set_first_room(sfVector2i *first, int size, room_t ***donjon)
{
	room_t *room = NULL;

	first->x = size / 2;
	first->y = size / 2;
	room = donjon[first->y][first->x];
	room->id = MAIN;
	return (room);
}

static room_t	***create_donjon(int size)
{
	room_t ***donjon = init_donjon_room(size);
	sfVector2i first;
	sfVector2i pos;

	if (donjon == NULL)
		return (NULL);
	set_first_room(&first, size, donjon);
	pos.x = first.x;
	pos.y = first.y;
	create_up_room_solo(donjon, pos, size);
	create_right_room_solo(donjon, pos, size);
	create_left_room_solo(donjon, pos, size);
	create_down_room_solo(donjon, pos, size);
	find_boss_room(donjon, size);
	find_shop_room(donjon, size);
	find_mini_boss_room(donjon, size);
	return (donjon);
}

donjon_t *init_donjon(int size)
{
	donjon_t *donjon = malloc(sizeof(donjon_t));

	if (donjon == NULL)
		return (NULL);
	donjon->rooms = create_donjon(size);
	if (donjon->rooms == NULL) {
		free(donjon);
		return (NULL);
	}
	donjon->pos = (sfVector2i){size / 2, size / 2};
	donjon->floor = size;
	return (donjon);
}
