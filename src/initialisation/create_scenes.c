/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to create scenes
*/

#include "my_rpg.h"
#include "scene.h"

void	add_button_menu(scene_t *scene)
{
	scene->buttons = create_menu_button((sfVector2f){685, 515},
	(sfVector2f){200, 50});
	scene->buttons->callback = &play_button;
	scene->buttons->next = create_menu_button((sfVector2f){985, 515},
	(sfVector2f){200, 50});
	scene->buttons->next->callback = &play_button;
	scene->buttons->next->next = create_menu_button((sfVector2f){835, 615},
	(sfVector2f){200, 50});
	scene->buttons->next->next->callback = &quit_button;
	scene->buttons->next->next->next = create_menu_button(
	(sfVector2f){10, 10}, (sfVector2f){200, 50});
	scene->buttons->next->next->next->callback = &settings_button;
	scene->buttons->next->next->next->next = create_menu_button(
	(sfVector2f){1710, 10}, (sfVector2f){200, 50});
	scene->buttons->next->next->next->next->callback = &how_to_play_button;
}

static scene_t	*create_menu(void)
{
	scene_t *scene = malloc(sizeof(scene_t));

	if (scene == NULL)
		return (NULL);
	add_button_menu(scene);
	return (scene);
}

static scene_t	*create_pause_menu(void)
{
	scene_t *scene = malloc(sizeof(scene_t));

	if (scene == NULL)
		return (NULL);
	scene->buttons = create_pause_button((sfVector2f){910, 415},
	(sfVector2f){200, 50});
	scene->buttons->callback = &resume_button;
	scene->buttons->next = create_pause_button((sfVector2f){910, 515},
	(sfVector2f){200, 50});
	scene->buttons->next->callback = &settings_button;
	scene->buttons->next->next = create_pause_button((sfVector2f){910, 615},
	(sfVector2f){200, 50});
	scene->buttons->next->next->callback = &quit_button;
	return (scene);
}

scene_t	**create_scenes(void)
{
	scene_t **scenes = malloc(sizeof(scene_t *) * 8);

	if (scenes == NULL) {
		return (NULL);
	}
	//scenes[0] = create_title_screen();
	scenes[1] = create_menu();
	scenes[3] = create_inventory();
	scenes[4] = create_pause_menu();
	scenes[5] = create_settings();
	scenes[6] = create_how_to_play();
	scenes[7] = create_level_up();
	return (scenes);
}
