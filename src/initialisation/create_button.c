/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** create button
*/

#include "my_rpg.h"
#include "scene.h"

sfTexture	*get_button_texture(int choice)
{
	static	sfTexture *menu = NULL;
	static	sfTexture *pause = NULL;

	if (menu == NULL) {
		menu = sfTexture_createFromFile("assets/menu_button.png", NULL);
		pause = sfTexture_createFromFile("assets/pause_button.png", NULL);
	} else if (choice == -1) {
		sfTexture_destroy(menu);
		sfTexture_destroy(pause);
	}
	if (choice == 1)
		return (menu);
	else if (choice == 2)
		return (pause);
	else
		return (NULL);
}

button_t	*create_button(sfVector2f pos, sfVector2f size)
{
	button_t *new_button = malloc(sizeof(button_t));

	new_button->rect = sfRectangleShape_create();
	sfRectangleShape_setPosition(new_button->rect, pos);
	sfRectangleShape_setSize(new_button->rect, size);
	sfRectangleShape_setFillColor(new_button->rect, sfWhite);
	new_button->state = IDLE;
	new_button->next = NULL;
	return (new_button);
}

button_t	*create_menu_button(sfVector2f pos, sfVector2f size)
{
	button_t *new_button = malloc(sizeof(button_t));
	sfTexture *texture = get_button_texture(1);

	new_button->rect = sfRectangleShape_create();
	sfRectangleShape_setPosition(new_button->rect, pos);
	sfRectangleShape_setSize(new_button->rect, size);
	sfRectangleShape_setTexture(new_button->rect, texture, sfFalse);
	new_button->state = IDLE;
	new_button->next = NULL;
	return (new_button);
}

button_t	*create_pause_button(sfVector2f pos, sfVector2f size)
{
	button_t *new_button = malloc(sizeof(button_t));
	sfTexture *texture = get_button_texture(2);

	new_button->rect = sfRectangleShape_create();
	sfRectangleShape_setPosition(new_button->rect, pos);
	sfRectangleShape_setSize(new_button->rect, size);
	sfRectangleShape_setTexture(new_button->rect, texture, sfFalse);
	new_button->state = IDLE;
	new_button->next = NULL;
	return (new_button);
}
