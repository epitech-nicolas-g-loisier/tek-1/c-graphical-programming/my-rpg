/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** destroy the entity
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "entity.h"

static void	destroy_clocks(entity_t *entity)
{
	if (entity->stat.high_frames)
		sfClock_destroy(entity->stat.high_frames);
	if (entity->clock)
		sfClock_destroy(entity->clock);
}

void	destroy_entity(entity_t *entity)
{
	if (!entity)
		return;
	destroy_clocks(entity);
	if (entity->rect)
		sfRectangleShape_destroy(entity->rect);
	free(entity);
}

void	destroy_entity_list(entity_t *entity_list)
{
	entity_t *next_entity = entity_list;

	while (next_entity) {
		next_entity = entity_list->next;
		destroy_entity(entity_list);
		entity_list = next_entity;
	}
}