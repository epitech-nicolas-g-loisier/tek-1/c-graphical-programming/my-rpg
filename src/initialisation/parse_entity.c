/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** parse entity and create an entity list
*/

#include "entity.h"
#include "my.h"

static	int get_data(char *data, int *idx)
{
	int value = 0;

	if (data == NULL || idx == NULL)
		return (0);
	else
		value = my_getnbr(&data[*idx]);
	while (data[*idx] != ':' && data[*idx] != '\0') {
		*idx += 1;
	}
	if (data[*idx] == ':')
		*idx += 1;
	return (value);
}

static	void modif_stat(entity_t *entity, char *data, int idx)
{
	while (data[idx] != '\0') {
		if (my_strncmp("life", &data[idx], 4) == 0) {
			entity->stat.max_health = get_data(data, &idx);
			entity->stat.health = entity->stat.max_health;
		} else if (my_strncmp("att", &data[idx], 3) == 0) {
			entity->stat.attack = get_data(data, &idx);
		}
	}
}

static	texture_list_t *get_texture(texture_list_t *choice, int id)
{
	int count = 0;

	while (count < id) {
		choice = choice->next;
		count++;
	}
	return (choice);
}

static	entity_t *get_next_entity_data(char *data, texture_list_t *texture,
					entity_t *entity)
{
	sfVector2f pos = {0, 0};
	int id = 0;

	id = get_data(data, &idx);
	pos.x = get_data(data, &idx);
	pos.y = get_data(data, &idx);
	entity->next = create_entity(type, pos, choice, scale);
	if (entity->next == NULL)
		return (entity->next);
	entity->next->id = id;
	modif_stat(entity->next, data, idx);
	return (entity->next);
}

int	parse_entity(char **list, entity_t *entity, texture_list_t *texture)
{
	int idx = 0;

	if (list == NULL || entity == NULL)
		return (84);
	if (my_strncmp(list[idx], "room", 4) == 0)
		idx++;
	while (list[idx] != NULL || my_strcmp(list[idx], "\n") == 0) {
		entity = get_next_entity_data(list[idx], texture, entity);
		if (entity == NULL)
			return (0);
		idx++;
	}
	entity->next = NULL;
	return (0);
}
