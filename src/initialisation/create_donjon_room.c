/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** create donjon room
*/

#include <dungeon.h>

int	room_check(room_t ***donjon, sfVector2i pos)
{
	int ret = check_big_square(donjon, pos);

	return (ret);
}

room_t	***create_up_room(room_t ***donjon, sfVector2i pos, int mod, int size)
{
	int rand_nb = 0;
	int new_mod = mod + (size);

	pos.y -= 1;
	if (pos.y < 0 || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	if (room_check(donjon, pos)) {
		return (donjon);
	}
	donjon[pos.y][pos.x]->id = ROOM;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_up_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_right_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_left_room(donjon, pos, new_mod, size) : NULL;
	return (donjon);
}

room_t ***create_right_room(room_t ***donjon, sfVector2i pos, int mod,
				int size)
{
	int rand_nb = 0;
	int new_mod = mod + (size);

	pos.x += 1;
	if (donjon[pos.y][pos.x] == NULL || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	if (room_check(donjon, pos)) {
		return (donjon);
	}
	donjon[pos.y][pos.x]->id = ROOM;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_up_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_right_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_down_room(donjon, pos, new_mod, size) : NULL;
	return (donjon);
}

room_t ***create_down_room(room_t ***donjon, sfVector2i pos, int mod, int size)
{
	int rand_nb = 0;
	int new_mod = mod + (size);

	pos.y += 1;
	if (donjon[pos.y] == NULL || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	if (room_check(donjon, pos)) {
		return (donjon);
	}
	donjon[pos.y][pos.x]->id = ROOM;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_left_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_right_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_down_room(donjon, pos, new_mod, size) : NULL;
	return (donjon);
}

room_t ***create_left_room(room_t ***donjon, sfVector2i pos, int mod, int size)
{
	int rand_nb = 0;
	int new_mod = mod + (size);

	pos.x -= 1;
	if (pos.x < 0 || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	if (room_check(donjon, pos)) {
		return (donjon);
	}
	donjon[pos.y][pos.x]->id = ROOM;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_up_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_left_room(donjon, pos, new_mod, size) : NULL;
	rand_nb = rand() % mod;
	(rand_nb < size) ? create_down_room(donjon, pos, new_mod, size) : NULL;
	return (donjon);
}
