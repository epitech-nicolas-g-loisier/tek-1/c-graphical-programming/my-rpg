/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Initialize settings scene
*/

#include "my_rpg.h"

scene_t	*create_settings(void)
{
	scene_t *scene = malloc(sizeof(scene_t));

	if (scene == NULL)
		return (NULL);
	scene->buttons = create_menu_button((sfVector2f){10, 1020},
	(sfVector2f){200, 50});
	scene->buttons->callback = &resume_button;
	scene->buttons->next = create_menu_button((sfVector2f){710, 390},
	(sfVector2f){200, 50});
	scene->buttons->next->callback = &set_framerate;
	scene->buttons->next->next = create_menu_button(
	(sfVector2f){1010, 390}, (sfVector2f){200, 50});
	scene->buttons->next->next->callback = &set_framerate;
	scene->buttons->next->next->next = create_menu_button(
	(sfVector2f){710, 550}, (sfVector2f){200, 50});
	scene->buttons->next->next->next->callback = &set_vertical_sync;
	scene->buttons->next->next->next->next = create_menu_button(
	(sfVector2f){1010, 550}, (sfVector2f){200, 50});
	scene->buttons->next->next->next->next->callback = &set_vertical_sync;
	return (scene);
}
