/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Load monster, handle apparition and moving
*/

#include "my_rpg.h"
#include "entity.h"

texture_list_t	*add_texture_to_list(char *file, int inc)
{
	texture_list_t *new_texture = malloc(sizeof(texture_list_t));
	sfIntRect rect[2] = {{0, 0, 38, 36}, {0, 0, 28, 45}};

	if (new_texture == NULL)
		return (NULL);
	new_texture->img = sfTexture_createFromFile(file, NULL);
	new_texture->rect = rect[inc];
	return (new_texture);
}

void	init_monsters(entity_t *monster, int id)
{
	sfVector2f origin[2] = {{19, 0}, {15, 0}};

	sfRectangleShape_setOrigin(monster->rect, origin[id]);
	monster->clock = sfClock_create();
}

entity_t	*stock_monsters(entity_t *list, texture_list_t *texture,
				int size)
{
	int inc = 0;
	char file[21] = "assets/monster_0.png";
	sfVector2f pos = {0, 0};

	while (inc != size) {
		texture->next = add_texture_to_list(file, inc);
		list->next = create_entity(MOB, pos, texture->next,
		(sfVector2f){3, 3});
		if (list->next == NULL)
			return (NULL);
		init_monsters(list->next, inc);
		define_monster_stat(list->next);
		texture = texture->next;
		texture->next = NULL;
		list = list->next;
		file[15]++;
		inc++;
	}
	return (list);
}

int	call_monster(sfRenderWindow *window, entity_t *player)
{
	static entity_t monster;
	static texture_list_t texture;
	static int call = 0;

	if (call == 0) {
		stock_monsters(&monster, &texture, 2);
		call = 1;
	}
	update_monsters(window, monster.next, player);
	return (0);
}
