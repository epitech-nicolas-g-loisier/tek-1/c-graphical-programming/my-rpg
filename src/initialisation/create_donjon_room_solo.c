/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** create donjon room
*/

#include <dungeon.h>

room_t ***create_up_room_solo(room_t ***donjon, sfVector2i pos, int size)
{
	int rand_nb = 0;
	int mod = size * 2;

	pos.y -= 1;
	if (pos.y < 0 || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	donjon[pos.y][pos.x]->id = ROOM;
	donjon[pos.y][pos.x + 1]->id = BLOCK;
	donjon[pos.y][pos.x - 1]->id = BLOCK;
	(rand_nb < size) ? create_up_room(donjon, pos, mod, size) : NULL;
	return (donjon);
}

room_t ***create_right_room_solo(room_t ***donjon, sfVector2i pos, int size)
{
	int rand_nb = 0;
	int mod = size * 2;

	pos.x += 1;
	if (donjon[pos.y][pos.x] == NULL || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	donjon[pos.y][pos.x]->id = ROOM;
	donjon[pos.y + 1][pos.x]->id = BLOCK;
	donjon[pos.y - 1][pos.x]->id = BLOCK;
	(rand_nb < size) ? create_right_room(donjon, pos, mod, size) : NULL;
	return (donjon);
}

room_t ***create_down_room_solo(room_t ***donjon, sfVector2i pos, int size)
{
	int rand_nb = 0;
	int mod = size * 2;

	pos.y += 1;
	if (donjon[pos.y] == NULL || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	donjon[pos.y][pos.x]->id = ROOM;
	donjon[pos.y][pos.x + 1]->id = BLOCK;
	donjon[pos.y][pos.x - 1]->id = BLOCK;
	(rand_nb < size) ? create_down_room(donjon, pos, mod, size) : NULL;
	return (donjon);
}

room_t ***create_left_room_solo(room_t ***donjon, sfVector2i pos, int size)
{
	int rand_nb = 0;
	int mod = size * 2;

	pos.x -= 1;
	if (pos.x < 0 || donjon[pos.y][pos.x]->id != VOID)
		return (donjon);
	donjon[pos.y][pos.x]->id = ROOM;
	donjon[pos.y + 1][pos.x]->id = BLOCK;
	donjon[pos.y - 1][pos.x]->id = BLOCK;
	(rand_nb < size) ? create_left_room(donjon, pos, mod, size) : NULL;
	return (donjon);
}
