/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Init inventory scene
*/

#include "my_rpg.h"

void	define_inventory_button(scene_t *scene)
{
	scene->buttons = create_button((sfVector2f){1160, 340},
	(sfVector2f){100, 50});
	scene->buttons->callback = &resume_button;
	scene->buttons->next = create_button((sfVector2f){665, 635},
	(sfVector2f){200, 100});
	sfRectangleShape_setOutlineThickness(scene->buttons->next->rect, 5);
	sfRectangleShape_setOutlineColor(scene->buttons->next->rect, sfCyan);
	scene->buttons->next->callback = &resume_button;
}

scene_t	*create_inventory(void)
{
	scene_t *scene = malloc(sizeof(scene_t));

	scene->objects = malloc(sizeof(object_t));
	if (scene == NULL || scene->objects == NULL)
		return (NULL);
	scene->objects->rect = sfRectangleShape_create();
	sfRectangleShape_setSize(scene->objects->rect,
	(sfVector2f){600, 400});
	sfRectangleShape_setFillColor(scene->objects->rect, sfBlack);
	sfRectangleShape_setPosition(scene->objects->rect,
	(sfVector2f){660, 340});
	define_inventory_button(scene);
	return (scene);
}
