/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** free the donjon
*/

#include "dungeon.h"

static	void free_room(room_t *room)
{
	free(room);
}

void	free_donjon(donjon_t *donjon)
{
	for (int idx_y = 0; donjon->rooms[idx_y] != NULL; idx_y++) {
		for (int x = 0; donjon->rooms[idx_y][x] != NULL; x++) {
			free_room(donjon->rooms[idx_y][x]);
		}
		free(donjon->rooms[idx_y]);
	}
	free(donjon->rooms);
	free(donjon);
}
