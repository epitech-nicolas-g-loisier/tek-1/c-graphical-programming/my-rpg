/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Display more scenes
*/

#include "my_rpg.h"

void	draw_how_to_play(sfRenderWindow *window, scene_t *scene,
			int *index_scene)
{
	char *move = "Movement:\n\t-Z : go up\n\t-Q : go left\n\t-S : g" \
			"o Down\n\t-D : go right\n";
	char *attack = "Atack:\n\t-F : attack\n";
	char *other = "Other:\n\t-I : open inventory\n\t-Escape : pause game\n";
	button_t *tmp = scene->buttons;

	sfRenderWindow_clear(window, sfBlack);
	display_text(window, (sfVector2f){50, 50}, move);
	display_text(window, (sfVector2f){50, 220}, attack);
	display_text(window, (sfVector2f){50, 305}, other);
	while (tmp != NULL) {
		sfRenderWindow_drawRectangleShape(window, tmp->rect, NULL);
		manage_button(tmp, window, index_scene);
		tmp = tmp->next;
	}
	display_text(window, (sfVector2f){1775, 1030}, "Menu");
}

void	set_settings_text(sfRenderWindow *window)
{
	display_text(window, (sfVector2f){755, 400}, "Low FPS");
	display_text(window, (sfVector2f){1050, 400}, "High FPS");
	display_text(window, (sfVector2f){740, 560}, "VSYNC ON");
	display_text(window, (sfVector2f){1035, 560}, "VSYNC OFF");
	display_text(window, (sfVector2f){75, 1030}, "Exit");
}

static	void	draw_settings(sfRenderWindow *window, int *index_scene,
				scene_t *scene, int old_scene)
{
	button_t *tmp = scene->buttons;
	int inc = 0;

	sfRenderWindow_clear(window, sfBlack);
	while (tmp != NULL) {
		sfRenderWindow_drawRectangleShape(window, tmp->rect, NULL);
		if (inc == 0) {
			if (old_scene == 1)
				tmp->callback = &menu_button;
			else
				tmp->callback = &resume_button;
			manage_button(tmp, window, index_scene);
		} else {
			manage_settings_button(tmp, window, inc);
		}
		tmp = tmp->next;
		inc++;
	}
	set_settings_text(window);
}

void	draw_level_up(sfRenderWindow *window, int *index_scene, scene_t *scene)
{
	button_t *tmp = scene->buttons;

	*index_scene = 7;
	sfRenderWindow_setMouseCursorVisible(window, sfTrue);
	sfRenderWindow_drawRectangleShape(window, scene->objects->rect, NULL);
	display_text(window, (sfVector2f){890, 470}, "Level Up!");
	while (tmp != NULL) {
		sfRenderWindow_drawRectangleShape(window, tmp->rect, NULL);
		manage_button(tmp, window, index_scene);
		tmp = tmp->next;
	}
}

void	call_other_scene(sfRenderWindow *window, int *index_scene,
			rpg_t *rpg)
{
	static int old_scene = 0;

	if (*index_scene == 6) {
		draw_how_to_play(window, rpg->scenes[6], index_scene);
	} else {
		if (*index_scene == 5) {
			draw_settings(window, index_scene, rpg->scenes[5],
			old_scene);
		}
		else if (*index_scene != 5) {
			old_scene = *index_scene;
		}
	}
}
