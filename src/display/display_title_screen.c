/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Display Title Screen
*/

#include <SFML/Graphics.h>

static void disp_title_screen(sfClock *clock, sfSprite *sprite,
				sfRenderWindow *window)
{
	sfTime time = sfClock_getElapsedTime(clock);
	sfColor color = {0, 0, 0, 255};

	while (time.microseconds < 2000000) {
		if (sfKeyboard_isKeyPressed(sfKeyReturn) == sfTrue)
			break;
		sfSprite_setColor(sprite, color);
		time = sfClock_getElapsedTime(clock);
		sfRenderWindow_drawSprite(window, sprite, NULL);
		sfRenderWindow_display(window);
		if (color.r < 250) {
			color.r += 4;
			color.g += 4;
			color.b += 4;
		}
	}
}

static void display_title(sfClock *clock, sfSprite *sprite,
				sfRenderWindow *window)
{
	sfTime time = sfClock_getElapsedTime(clock);
	sfColor color = {255, 255, 255, 0};

	while (time.microseconds < 4000000) {
		if (time.microseconds < 2000000)
			break;
		else if (sfKeyboard_isKeyPressed(sfKeyReturn) == sfTrue)
			break;
		sfSprite_setColor(sprite, color);
		time = sfClock_getElapsedTime(clock);
		sfRenderWindow_drawSprite(window, sprite, NULL);
		sfRenderWindow_display(window);
		if (color.a < 250) {
			color.a += 3;
		}
	}
}

void	display_title_screen_anim(sfRenderWindow *window)
{
	char *name1 = "assets/Title_screen.jpg";
	char *name2 = "assets/Title.png";
	sfClock *clock = sfClock_create();
	sfTexture *texture1 = sfTexture_createFromFile(name1, NULL);
	sfTexture *texture2 = sfTexture_createFromFile(name2, NULL);
	sfSprite *title_screen = sfSprite_create();
	sfSprite *title = sfSprite_create();
	sfVector2f pos = {360, 295};

	sfSprite_setTexture(title_screen, texture1, sfFalse);
	sfSprite_setTexture(title, texture2, sfFalse);
	sfSprite_setPosition(title, pos);
	disp_title_screen(clock, title_screen, window);
	display_title(clock, title, window);
	sfClock_destroy(clock);
	sfSprite_destroy(title_screen);
	sfSprite_destroy(title);
	sfTexture_destroy(texture1);
	sfTexture_destroy(texture2);
}

static	void display_sprite(sfRenderWindow *window, sfTexture *texture1,
				sfTexture *texture2)
{
	sfVector2f pos = {360, 295};
	sfSprite *title_screen = sfSprite_create();
	sfSprite *title = sfSprite_create();

	sfSprite_setTexture(title_screen, texture1, sfFalse);
	sfSprite_setTexture(title, texture2, sfFalse);
	sfSprite_setPosition(title, pos);
	sfRenderWindow_drawSprite(window, title_screen, NULL);
	sfRenderWindow_drawSprite(window, title, NULL);
	sfSprite_destroy(title_screen);
	sfSprite_destroy(title);
}

void	display_title_screen(sfRenderWindow *window, int choice)
{
	char *name1 = "assets/Title_screen.jpg";
	char *name2 = "assets/Title.png";
	static sfTexture *texture1 = NULL;
	static sfTexture *texture2 = NULL;

	if (choice == 1) {
		if (texture1 == NULL) {
			texture1 = sfTexture_createFromFile(name1, NULL);
			texture2 = sfTexture_createFromFile(name2, NULL);
		}
		display_sprite(window, texture1, texture2);
	} else {
		sfTexture_destroy(texture1);
		sfTexture_destroy(texture2);
		texture1 = NULL;
		texture2 = NULL;
	}
}
