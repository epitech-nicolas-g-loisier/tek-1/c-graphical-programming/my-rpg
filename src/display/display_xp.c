/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** display xp bar
*/

#include "my_rpg.h"
#include "my.h"

static void	init_rect_shape(sfRectangleShape *xp, sfTexture *texture)
{
	sfIntRect rect = {0, 0, 364, 10};
	sfVector2f pos = {(X_MID - 364), (HEIGHT - 40)};

	sfRectangleShape_setPosition(xp, pos);
	sfRectangleShape_setTexture(xp, texture, sfFalse);
	sfRectangleShape_setTextureRect(xp, rect);
	sfRectangleShape_setSize(xp, (sfVector2f){364, 10});
	sfRectangleShape_scale(xp, (sfVector2f){2, 2});
}

static	void create_xp_str(int xp, int max_xp, char *str)
{
	int idx = 0;

	while (max_xp != 0) {
		str[idx] = (max_xp % 10 + '0');
		max_xp = max_xp / 10;
		idx++;
	}
	str[idx] = ' ';
	str[idx + 1] = '/';
	str[idx + 2] = ' ';
	idx += 3;
	if (xp == 0)
		str[idx++] = '0';
	while (xp != 0) {
		str[idx] = (xp % 10 + '0');
		xp = xp / 10;
		idx++;
	}
	str[idx] = '\0';
	str = my_revstr(str);
}

static	void display_xp_nb(sfRenderWindow *window, int xp, int max_xp)
{
	sfVector2f pos = {(X_MID + 374), (HEIGHT - 42.5)};
	sfFont *font = sfFont_createFromFile("assets/sonic_comic.ttf");
	sfText *text = sfText_create();
	char string[30];

	create_xp_str(xp, max_xp, &string[0]);
	sfText_setPosition(text, pos);
	sfText_setFont(text, font);
	sfText_setCharacterSize(text, 20);
	sfText_setColor(text, sfGreen);
	sfText_setString(text, string);
	sfRenderWindow_drawText(window, text, NULL);
	sfFont_destroy(font);
	sfText_destroy(text);
}

static	void calc_new_rect(sfIntRect *rect, int xp, int max_xp)
{
	double ratio = xp;

	ratio = ratio / max_xp;
	rect->top += 10;
	rect->width = rect->width * ratio;
}

int	display_xp(sfRenderWindow *window, int xp, int max_xp)
{
	sfTexture *texture = NULL;
	sfRectangleShape *xp_bar = sfRectangleShape_create();
	sfIntRect rect = {0, 0, 364, 10};

	texture = sfTexture_createFromFile("assets/xp_bar.png", NULL);
	init_rect_shape(xp_bar, texture);
	sfRenderWindow_drawRectangleShape(window, xp_bar, NULL);
	calc_new_rect(&rect, xp, max_xp);
	sfRectangleShape_setTextureRect(xp_bar, rect);
	sfRectangleShape_setSize(xp_bar, (sfVector2f){rect.width, 10});
	sfRenderWindow_drawRectangleShape(window, xp_bar, NULL);
	sfTexture_destroy(texture);
	sfRectangleShape_destroy(xp_bar);
	display_xp_nb(window, xp, max_xp);
	return (0);
}
