/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** display pegi 18
*/

#include <SFML/Graphics.h>

static void display_begin(sfClock *clock, sfSprite *sprite,
				sfRenderWindow *window)
{
	sfTime time = sfClock_getElapsedTime(clock);
	sfColor color = {25, 25, 25, 255};

	while (time.microseconds < 2500000) {
		if (sfKeyboard_isKeyPressed(sfKeyReturn) == sfTrue)
			break;
		sfSprite_setColor(sprite, color);
		time = sfClock_getElapsedTime(clock);
		sfRenderWindow_drawSprite(window, sprite, NULL);
		sfRenderWindow_display(window);
		if (color.r < 250) {
			color.r += 4;
			color.g += 4;
			color.b += 4;
		}
	}
}

static void display_end(sfClock *clock, sfSprite *sprite,
			sfRenderWindow *window)
{
	sfTime time = sfClock_getElapsedTime(clock);
	sfColor color = {255, 255, 255, 255};

	while (time.microseconds < 4500000) {
		if (time.microseconds < 2500000)
			break;
		else if (sfKeyboard_isKeyPressed(sfKeyReturn) == sfTrue)
				break;
		sfSprite_setColor(sprite, color);
		time = sfClock_getElapsedTime(clock);
		sfRenderWindow_drawSprite(window, sprite, NULL);
		sfRenderWindow_display(window);
		if (color.r > 4) {
			color.r -= 4;
			color.g -= 4;
			color.b -= 4;
		}
	}
}

void	display_pegi(sfRenderWindow *window)
{
	char *name = "assets/pegi_18.png";
	sfClock *clock = sfClock_create();
	sfTexture *texture = sfTexture_createFromFile(name, NULL);
	sfTime time = sfClock_getElapsedTime(clock);
	sfSprite *sprite = sfSprite_create();
	sfVector2f pos = {(960) - 325, (540) - 325};

	sfSprite_setTexture(sprite, texture, sfFalse);
	sfSprite_setPosition(sprite, pos);
	display_begin(clock, sprite, window);
	display_end(clock, sprite, window);
	sfRenderWindow_clear(window, sfBlack);
	while (time.microseconds < 50000) {
		time = sfClock_getElapsedTime(clock);
	}
	sfClock_destroy(clock);
	sfSprite_destroy(sprite);
	sfTexture_destroy(texture);
}
