/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** display text at given pos
*/

#include <SFML/Graphics.h>

void	display_text(sfRenderWindow *window, sfVector2f pos, char *str)
{
	sfFont *font = sfFont_createFromFile("assets/sonic_comic.ttf");
	sfText *text = sfText_create();

	sfText_setPosition(text, pos);
	sfText_setFont(text, font);
	sfText_setCharacterSize(text, 25);
	sfText_setColor(text, sfWhite);
	sfText_setString(text, str);
	sfRenderWindow_drawText(window, text, NULL);
	sfFont_destroy(font);
	sfText_destroy(text);
}
