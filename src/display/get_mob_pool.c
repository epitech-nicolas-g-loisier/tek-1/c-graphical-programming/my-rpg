/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Handles mob pool requests
*/

#include <time.h>
#include <stdbool.h>
#include <stdlib.h>

static void	destroy_pool(char **pool)
{
	int count = 0;

	if (!pool)
		return;
	while (pool[count]) {
		free(pool[count]);
		count++;
	}
	free(pool);
}

static char	**hold_mob_pool(char **mob_pool, int pool_id,
				bool do_destroy)
{
	static char **keep_mob_pool[3] = {NULL};

	if (mob_pool)
		keep_mob_pool[pool_id] = mob_pool;
	else {
		if (do_destroy) {
			destroy_pool(keep_mob_pool[pool_id]);
			keep_mob_pool[pool_id] = NULL;
		}
		return (keep_mob_pool[pool_id]);
	}
	return (NULL);
}

void	clear_mob_pool(void)
{
	for (int i = 0; i < 4; i++)
		hold_mob_pool(NULL, i, true);
}

char	*get_mob_from_pool(int pool_id)
{
	char **pool = NULL;
	int random_value = rand() % 10;
	int mob_id = 0;

	pool = hold_mob_pool(NULL, pool_id, false);
	if (!pool[0])
		return (NULL);
	for (int i = 0; i < random_value; i++) {
		while (pool[mob_id] && pool[mob_id][0] != '\n')
			mob_id++;
		if (!pool[mob_id])
			mob_id = 0;
	}
	return (pool[mob_id]);
}

int	create_mob_pool(char *filepath, int pool_id)
{
	char **mob_pool = read_file(filepath);

	hold_mob_pool(NULL, pool_id, true);
	if (!mob_pool)
		return (84);
	hold_mob_pool(mob_pool, pool_id, false);
	return (0);
}