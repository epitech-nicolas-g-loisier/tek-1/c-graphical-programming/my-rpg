/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Displays the player's health
*/

#include <SFML/Graphics.h>

static int	destroy_objects(sfTexture *texture, sfRectangleShape *rect)
{
	sfTexture_destroy(texture);
	sfRectangleShape_destroy(rect);
	return (0);
}
static void	display_hearts(sfRenderWindow *window, int hp,
				sfRectangleShape *heart)
{
	sfVector2f pos = {50, 40};

	sfRectangleShape_setPosition(heart, pos);
	for (int i = 0; i < hp / 2; i++) {
		sfRenderWindow_drawRectangleShape(window, heart, NULL);
		pos.x = 50 * ((i + 1) % 10 + 1);
		pos.y = 40 + 40 * ((i + 1) / 10);
		sfRectangleShape_setPosition(heart, pos);
	}
}

static void	display_half_a_heart(sfRenderWindow *window, int hp,
				sfRectangleShape *heart)
{
	sfVector2f pos;

	pos.x = 50 * (hp / 2 % 10 + 1);
	pos.y = 40 + 40 * (hp / 20);
	if (hp > 0 && hp / 2 * 2 != hp) {
		sfRectangleShape_setPosition(heart, pos);
		sfRenderWindow_drawRectangleShape(window, heart, NULL);
	}
}

static void	init_rect_shape(sfRectangleShape *heart, sfTexture *texture)
{
	sfIntRect rect = {0, 0, 17, 16};

	sfRectangleShape_setTexture(heart, texture, sfFalse);
	sfRectangleShape_setTextureRect(heart, rect);
	sfRectangleShape_setSize(heart, (sfVector2f){17, 16});
	sfRectangleShape_scale(heart, (sfVector2f){2, 2});
}

int	display_health(sfRenderWindow *window, int curr_hp, int max_hp)
{
	sfTexture *texture = NULL;
	sfRectangleShape *heart = sfRectangleShape_create();
	sfIntRect rect = {0, 0, 17, 16};

	if (!heart)
		return (84);
	texture = sfTexture_createFromFile("assets/heart.png", NULL);
	if (!texture) {
		sfRectangleShape_destroy(heart);
		return (84);
	}
	init_rect_shape(heart, texture);
	display_hearts(window, max_hp, heart);
	rect.left += rect.width;
	sfRectangleShape_setTextureRect(heart, rect);
	display_hearts(window, curr_hp, heart);
	rect.left += rect.width;
	sfRectangleShape_setTextureRect(heart, rect);
	display_half_a_heart(window, curr_hp, heart);
	return (destroy_objects(texture, heart));
}