/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Handle end game animation and quit the game
*/

#include "my_rpg.h"
#include "scene.h"

sfRectangleShape	*init_end_game(void)
{
	sfTexture *texture = sfTexture_createFromFile("assets/end_game.png", NULL);
	sfRectangleShape *rect = sfRectangleShape_create();

	sfRectangleShape_setTexture(rect, texture, sfFalse);
	sfRectangleShape_setSize(rect, (sfVector2f){1920, 1080});
	return (rect);
}

void	launch_end_game(sfRenderWindow *window)
{
	static int call = 0;
	static sfClock *clock = NULL;
	static sfRectangleShape *rect = NULL;

	if (call == 0) {
		rect = init_end_game();
		clock = sfClock_create();
		call = 1;
	}
	sfRenderWindow_drawRectangleShape(window, rect, NULL);
	if (sfClock_getElapsedTime(clock).microseconds > 1000000) {
		sfRenderWindow_close(window);
	}
}
