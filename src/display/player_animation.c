/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to move player's texture rect
*/

#include "entity.h"

static void	adapt_scale(entity_t *link, int new_x_scale)
{
	sfVector2f scale = sfRectangleShape_getScale(link->rect);
	sfVector2f origin = {0, 0};
	sfVector2f size = sfRectangleShape_getSize(link->rect);

	if ((new_x_scale == -1 && scale.x > 0) ||
		(new_x_scale == 1 && scale.x < 0)) {
		scale.x *= -1;
		sfRectangleShape_setScale(link->rect, scale);
		origin.x = (new_x_scale == -1 ? size.x : 0);
		sfRectangleShape_setOrigin(link->rect, origin);
	}
}

static void	start_walking(entity_t *link, sfIntRect hitbox)
{
	hitbox.width = 24;
	hitbox.left = 72;
	hitbox.height = 25;
	if (link->dir != 3) {
		hitbox.top = link->dir * 25;
		adapt_scale(link, 1);
	} else if (link->dir == 3) {
		hitbox.top = 25;
		adapt_scale(link, -1);
	}
	sfRectangleShape_setTextureRect(link->rect, hitbox);
	if (link->clock)
		sfClock_restart(link->clock);
	else
		link->clock = sfClock_create();
}

static void	move_texture_walking(entity_t *link, sfIntRect hitbox)
{
	if (sfClock_getElapsedTime(link->clock).microseconds > 40000) {
		hitbox.left += 24;
		sfRectangleShape_setTextureRect(link->rect, hitbox);
		sfClock_restart(link->clock);
	}
}

void	player_walk(entity_t *link)
{
	static int curr_dir = 0;
	sfKeyCode key_list[4] = {sfKeyS, sfKeyD, sfKeyZ, sfKeyQ};
	sfIntRect hitbox = sfRectangleShape_getTextureRect(link->rect);

	if (hitbox.left < 72 ||
		sfKeyboard_isKeyPressed(key_list[curr_dir]) == sfFalse) {
		curr_dir = link->dir;
		start_walking(link, hitbox);
		return;
	}
	move_texture_walking(link, hitbox);
	if (hitbox.left >= 288) {
		hitbox.left = 72;
		sfRectangleShape_setTextureRect(link->rect, hitbox);
	}
}
