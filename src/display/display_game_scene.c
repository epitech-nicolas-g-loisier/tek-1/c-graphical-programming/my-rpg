/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to draw scenes depending index_scene
*/

#include "my_rpg.h"
#include "scene.h"

static	void	display_hud(sfRenderWindow *window, donjon_t *donjon,
				entity_t *entity)
{
	draw_minimap(donjon, window);
	display_xp(window, entity->stat.xp, entity->stat.max_xp);
	display_health(window, entity->stat.health, entity->stat.max_health);
}

static void	display_game(sfRenderWindow *window, entity_t *entity_list,
				donjon_t *donjon)
{
	static int begin = 0;

	if (begin == 0) {
		display_begin(window);
		begin = 1;
	}
	sfRenderWindow_clear(window, sfBlack);
	sfRenderWindow_setMouseCursorVisible(window, sfFalse);
	draw_background(window, donjon, (sfVector2f){0, 0});
	check_collision(entity_list);
	manage_player(entity_list);
	call_monster(window, entity_list);
	draw_entities(window, entity_list);
	move_room(window, donjon, entity_list);
	display_hud(window, donjon, entity_list);
}

static void	display_inventory(sfRenderWindow *window, scene_t *scene,
				int *index_scene)
{
	button_t *tmp = scene->buttons;

	sfRenderWindow_drawRectangleShape(window,
	scene->objects->rect,NULL);
	while (tmp != NULL) {
		sfRenderWindow_setMouseCursorVisible(window, sfTrue);
		sfRenderWindow_drawRectangleShape(window, tmp->rect, NULL);
		manage_button(tmp, window, index_scene);
		tmp = tmp->next;
	}
}

void	display_game_scene(sfRenderWindow *window, rpg_t *rpg,
			donjon_t *donjon, entity_t *entity)
{
	if (rpg->index_scene == 2) {
		display_game(window, entity, donjon);
	}
	else if (rpg->index_scene == 3) {
		display_inventory(window, rpg->scenes[3], &rpg->index_scene);
	}
}
