/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** display begin
*/

#include "my_rpg.h"

void	display_begin(sfRenderWindow *window)
{
	char *str = "Link, beat Ganondorf and save the princess Zelda\n";

	while (sfKeyboard_isKeyPressed(sfKeyReturn) != sfTrue) {
		sfRenderWindow_clear(window, sfBlack);
		display_text(window, (sfVector2f){600, Y_MID}, str);
		display_text(window, (sfVector2f){(WIDTH) - 400, (HEIGHT) - 50},
		"press Return to play\n");
		sfRenderWindow_display(window);
	}
}
