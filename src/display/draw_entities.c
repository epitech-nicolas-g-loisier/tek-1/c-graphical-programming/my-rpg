/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** draw the entities
*/

#include <stdlib.h>
#include "entity.h"

static void	swap_items(sfRectangleShape **rect_list,
			sfVector2f *pos_list)
{
	sfRectangleShape *rect_hold = *rect_list;
	sfVector2f pos_hold = *pos_list;

	pos_list[0] = pos_list[1];
	pos_list[1] = pos_hold;
	rect_list[0] = rect_list[1];
	rect_list[1] = rect_hold;
}

static void	do_bubble_sort(sfRectangleShape **rect_list,
			sfVector2f *pos_list, int size)
{
	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - 1; j++) {
			if (pos_list[j].y > pos_list[j + 1].y)
				swap_items(&rect_list[j], &pos_list[j]);
		}
	}
}

static void	init_list(sfRectangleShape **rect_list,
			sfVector2f *pos_list, entity_t *entity_list)
{
	for (int i = 0; entity_list; i++) {
		rect_list[i] = entity_list->rect;
		pos_list[i] = sfRectangleShape_getPosition(entity_list->rect);
		entity_list = entity_list->next;
		if (!entity_list) {
			rect_list[i + 1] = NULL;
			pos_list[i + 1] = (sfVector2f){0, 0};
		}
	}
}

static sfRectangleShape	**sort_list(entity_t *entity_list)
{
	sfRectangleShape **sorted_list = NULL;
	sfVector2f *hitbox_list = NULL;
	entity_t *temp = entity_list;
	int size = 0;

	while (temp) {
		temp = temp->next;
		size++;
	}
	sorted_list = malloc(sizeof(sfRectangleShape *) * (size + 1));
	hitbox_list = malloc(sizeof(sfVector2f) * (size + 1));
	if (!sorted_list || !hitbox_list) {
		free(sorted_list);
		free(hitbox_list);
		return (NULL);
	}
	init_list(sorted_list, hitbox_list, entity_list);
	do_bubble_sort(sorted_list, hitbox_list, size);
	free(hitbox_list);
	return (sorted_list);
}

int	draw_entities(sfRenderWindow *window, entity_t *entity_list)
{
	sfRectangleShape **new_entity_list = sort_list(entity_list);

	if (!new_entity_list)
		return (84);
	for (int i = 0; entity_list; i++) {
		sfRenderWindow_drawRectangleShape(window, new_entity_list[i],
						NULL);
		entity_list = entity_list->next;
	}
	free(new_entity_list);
	return (0);
}