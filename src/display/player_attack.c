/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to handle attacks
*/

#include <unistd.h>
#include "dungeon.h"
#include "entity.h"

static void	adapt_scale(entity_t *link)
{
	sfVector2f origin = sfRectangleShape_getOrigin(link->rect);
	const sfVector2f origin_list[4] = {{11, 11}, {11, 8}, {11, 8}, {13, 8}};
	sfVector2f scale = sfRectangleShape_getScale(link->rect);

	origin.x += origin_list[link->dir].x;
	origin.y += origin_list[link->dir].y;
	sfRectangleShape_setOrigin(link->rect, origin);
	if (link->dir != 3) {
		scale.x *= (scale.x > 0) ? 1 : -1;
	} else if (link->dir == 3) {
		scale.x *= (scale.x < 0) ? 1 : -1;
	}
	sfRectangleShape_setScale(link->rect, scale);
}

static void	start_attack(entity_t *link, sfIntRect hitbox)
{
	hitbox.left = 0;
	if (link->dir != 3) {
		hitbox.top = 75 + link->dir * 46;
	}
	else if (link->dir == 3) {
		hitbox.top = 121;
	}
	hitbox.width = 46;
	hitbox.height = 46;
	sfRectangleShape_setTextureRect(link->rect, hitbox);
	sfRectangleShape_setSize(link->rect, (sfVector2f) {46, 46});
	adapt_scale(link);
	sfClock_restart(link->clock);
}

static void	move_texture_attack(entity_t *link, sfIntRect hitbox)
{
	if (sfClock_getElapsedTime(link->clock).microseconds > 17500) {
		hitbox.left += 46;
		sfRectangleShape_setTextureRect(link->rect, hitbox);
		sfClock_restart(link->clock);
	}
}

static void	reset_origin(entity_t *link)
{
	const sfVector2f origin_list[4] = {{11, 11}, {11, 8}, {11, 8}, {13, 8}};
	sfVector2f origin = sfRectangleShape_getOrigin(link->rect);

	origin.x -= origin_list[link->dir].x;
	origin.y -= origin_list[link->dir].y;
	sfRectangleShape_setOrigin(link->rect, origin);
}

void	player_attack(entity_t *link, entity_t *entity_list)
{
	sfIntRect hitbox = sfRectangleShape_getTextureRect(link->rect);

	if (hitbox.top < 75) {
		start_attack(link, hitbox);
		return;
	}
	move_texture_attack(link, hitbox);
	hitbox = sfRectangleShape_getTextureRect(link->rect);
	if (hitbox.left == 138) {
		attack_zone(link, entity_list);
	}
	else if (hitbox.left > 322) {
		link->status = 0;
		hitbox.width = 24;
		hitbox.height = 25;
		sfRectangleShape_setTextureRect(link->rect, hitbox);
		sfRectangleShape_setSize(link->rect, (sfVector2f) {24, 25});
		reset_origin(link);
		set_player_idle(link);
	}
}
