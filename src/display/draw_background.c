/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Draws the background of the room
*/

#include <SFML/Graphics.h>
#include "dungeon.h"

static void	draw_top_walls(sfRenderWindow *window, sfColor color,
				sfVector2f origin)
{
	sfRectangleShape *rect = sfRectangleShape_create();
	sfTexture *texture = sfTexture_createFromFile("assets/up.png", NULL);
	sfVector2f pos = {0, 0};
	sfVector2f size = {1920, 150};

	sfRectangleShape_setOrigin(rect, origin);
	sfRectangleShape_scale(rect, (sfVector2f){1, 1});
	sfRectangleShape_setFillColor(rect, color);
	sfRectangleShape_setPosition(rect, pos);
	sfRectangleShape_setSize(rect, size);
	sfRectangleShape_setTexture(rect, texture, sfFalse);
	sfRenderWindow_drawRectangleShape(window, rect, NULL);
	pos.y = 1080 - (origin.y * 2);
	sfRectangleShape_setPosition(rect, pos);
	sfRectangleShape_scale(rect, (sfVector2f){1, -1});
	sfRenderWindow_drawRectangleShape(window, rect, NULL);
	sfTexture_destroy(texture);
	sfRectangleShape_destroy(rect);
}

static void	draw_side_walls(sfRenderWindow *window, sfColor color,
				sfVector2f origin)
{
	sfRectangleShape *rect = sfRectangleShape_create();
	sfTexture *texture = sfTexture_createFromFile("assets/cote.png", NULL);
	sfVector2f pos = {0, 150};
	sfVector2f size = {150, 780};

	sfRectangleShape_setOrigin(rect, origin);
	sfRectangleShape_setPosition(rect, pos);
	sfRectangleShape_setFillColor(rect, color);
	sfRectangleShape_setSize(rect, size);
	sfRectangleShape_setTexture(rect, texture, sfFalse);
	sfRenderWindow_drawRectangleShape(window, rect, NULL);
	pos.x = 1920 - (origin.x * 2);
	sfRectangleShape_setPosition(rect, pos);
	sfRectangleShape_scale(rect, (sfVector2f){-1, 1});
	sfRenderWindow_drawRectangleShape(window, rect, NULL);
	sfTexture_destroy(texture);
	sfRectangleShape_destroy(rect);
}

static void	draw_floor(sfRenderWindow *window, sfVector2f origin)
{
	sfRectangleShape *rect = sfRectangleShape_create();
	sfTexture *texture = sfTexture_createFromFile("assets/ground.png",
							NULL);
	sfVector2f pos = {150, 150};
	sfVector2f size = {1620, 780};

	sfRectangleShape_setOrigin(rect, origin);
	sfRectangleShape_setTexture(rect, texture, sfFalse);
	sfRectangleShape_setPosition(rect, pos);
	sfRectangleShape_setSize(rect, size);
	sfRenderWindow_drawRectangleShape(window, rect, NULL);
	sfTexture_destroy(texture);
	sfRectangleShape_destroy(rect);
}

static	void draw_door(sfRenderWindow *window, room_t ***donjon, sfVector2i pos,
			sfVector2f origin)
{
	draw_up_door(window, donjon, pos, origin);
	draw_right_door(window, donjon, pos, origin);
	draw_down_door(window, donjon, pos, origin);
	draw_left_door(window, donjon, pos, origin);
}
void	draw_background(sfRenderWindow *window, donjon_t *donjon,
			sfVector2f origin)
{
	sfColor color = {125, 125, 125, 255};

	draw_floor(window, origin);
	draw_side_walls(window, color, origin);
	draw_top_walls(window, color, origin);
	draw_door(window, donjon->rooms, donjon->pos, origin);
}
