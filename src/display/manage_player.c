/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to attack, walk...
*/

#include "entity.h"
#include "dungeon.h"

void	set_player_idle(entity_t *link)
{
	sfIntRect hitbox = sfRectangleShape_getTextureRect(link->rect);

	if (hitbox.left < 72 && hitbox.top < 75) {
		return;
	}
	hitbox.left = 0;
	if (link->dir != 3) {
		hitbox.top = link->dir * 25;
	} else if (link->dir == 3) {
		hitbox.top = 25;
	}
	sfRectangleShape_setTextureRect(link->rect, hitbox);
	sfClock_restart(link->clock);
}

static int	get_eyes_pos(void)
{
	static sfClock *clock = NULL;
	static int between = 5000000;
	sfTime time;

	if (clock == NULL) {
		clock = sfClock_create();
	}
	time = sfClock_getElapsedTime(clock);
	if (time.microseconds > between && time.microseconds < between + 50000)
		return (1);
	else if (time.microseconds >= between + 50000 &&
	time.microseconds < between + 125000)
		return (2);
	else {
		if (time.microseconds >= between + 125000) {
			sfClock_restart(clock);
			between = (rand() % 10 + 2) * 1000000;
		}
	}
	return (0);
}

static void	blink_eyes(entity_t *link)
{
	sfIntRect hitbox = sfRectangleShape_getTextureRect(link->rect);
	int pos = get_eyes_pos();

	if (link->dir == 2) {
		return;
	}
	hitbox.left = pos * 24;
	sfRectangleShape_setTextureRect(link->rect, hitbox);
}

void	manage_player(entity_t *link)
{
	if (link->status == 0) {
		set_player_idle(link);
		blink_eyes(link);
	} else if (link->status == 1) {
		player_walk(link);
	} else {
		player_attack(link, link->next);
	}
}
