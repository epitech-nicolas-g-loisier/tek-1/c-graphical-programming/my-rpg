/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Play music
*/

#include <SFML/Graphics.h>
#include <SFML/Audio.h>

void	play_music(int choice)
{
	static sfMusic *music = NULL;

	if (choice == 0) {
		music = sfMusic_createFromFile("assets/SoundTrack.ogg");
		sfMusic_setLoop(music, sfTrue);
		sfMusic_play(music);
	} else
		sfMusic_destroy(music);
}
