/*
** EPITECH PROJECT, 2018
** My_rpg
** File description:
** Handle monsters animation
*/

#include "entity.h"

void	set_new_rect(entity_t *monster, sfIntRect *rect, int id)
{
	sfIntRect side_rect[2] = {{0, 0, 28, 45}, {0, 0, 64, 45}};
	int offset[2][2] = {{38, 38}, {28, 64}};
	int count = 0;

	if (monster->dir == 2 || monster->dir == 3) {
		count = 1;
	}
	if (id == 1) {
		rect->width = side_rect[count].width;
		rect->height = side_rect[count].height;
	}
	rect->left += offset[id][count];
	launch_monster_attack(monster, rect, count, id);
}

void	move_rect(entity_t *monster, int id)
{
	sfIntRect rect = sfRectangleShape_getTextureRect(monster->rect);
	sfVector2f scale = sfRectangleShape_getScale(monster->rect);
	sfVector2f size = {0, 0};

	if (monster->dir != 3) {
		rect.top = rect.height * monster->dir;
	}
	else if (monster->dir == 3 && scale.x > 0) {
		rect.top = rect.height * 2;
		scale.x *= -1;
	}
	set_new_rect(monster, &rect, id);
	size.x = rect.width;
	size.y = rect.height;
	sfRectangleShape_setSize(monster->rect, size);
	sfRectangleShape_setTextureRect(monster->rect, rect);
	sfRectangleShape_setScale(monster->rect, scale);
}

void	launch_moving_animation(entity_t *monster, int id)
{
	if (sfClock_getElapsedTime(monster->clock).microseconds > 100000) {
		sfClock_restart(monster->clock);
		move_rect(monster, id);
	}
}
