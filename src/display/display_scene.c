/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Functions used to draw scenes depending index_scene
*/

#include "my_rpg.h"
#include "scene.h"

static void	draw_title_screen(sfRenderWindow *window, int *index_scene)
{
	static int count = 0;
	sfFont *font = sfFont_createFromFile("assets/sonic_comic.ttf");
	sfText *text = sfText_create();
	sfColor color = sfWhite;

	if (count++ > 25 && count < 30)
		color = sfTransparent;
	else if (count > 30)
		count = 0;
	display_title_screen(window, 1);
	sfText_setPosition(text, (sfVector2f){(X_MID - 374), (Y_MID) + 300});
	sfText_setFont(text, font);
	sfText_setCharacterSize(text, 50);
	sfText_setColor(text, color);
	sfText_setString(text, "Press escape to continue\n");
	sfRenderWindow_drawText(window, text, NULL);
	sfFont_destroy(font);
	sfText_destroy(text);
	if (sfKeyboard_isKeyPressed(sfKeyEscape) == sfTrue)
		*index_scene = 1;
}

static void	draw_menu(sfRenderWindow *window, int *index_scene,
				scene_t *scene)
{
	char text[5][12] = {"New Game\0", "Continue\0", "Quit\0", "Setting\0",
				"How to Play\0"};
	sfVector2f pos[5] = {{717, 525}, {1015, 525}, {905, 625}, {50, 20},
				{1718, 20}};
	button_t *tmp = scene->buttons;
	int count = 0;
	sfColor color = {100, 100, 100, 255};

	sfRenderWindow_clear(window, sfBlack);
	while (tmp != NULL) {
		sfRenderWindow_drawRectangleShape(window, tmp->rect, NULL);
		display_text(window, pos[count], text[count]);
		if (count == 2) {
			manage_button_window(tmp, window);
		} else if (count == 1) {
			sfRectangleShape_setFillColor(tmp->rect, color);
		} else {
			manage_button(tmp, window, index_scene);
		}
		tmp = tmp->next;
		count++;
	}
	sfRenderWindow_setMouseCursorVisible(window, sfTrue);
}

static void	draw_pause_menu(sfRenderWindow *window, int *index_scene,
				scene_t *scene)
{
	char text[3][12] = {"Resume\0","Setting\0", "Quit\0"};
	sfVector2f pos[3] = {{955, 425}, {950, 525}, {975, 625}};
	button_t *tmp = scene->buttons;
	int count = 0;

	sfRenderWindow_setMouseCursorVisible(window, sfTrue);
	while (tmp != NULL) {
		sfRenderWindow_drawRectangleShape(window, tmp->rect, NULL);
		display_text(window, pos[count], text[count]);
		if (count != 2) {
			manage_button(tmp, window, index_scene);
		} else {
			manage_button_window(tmp, window);
		}
		tmp = tmp->next;
		count++;
	}
}

void	display_scene(sfRenderWindow *window, rpg_t *rpg,
			donjon_t *donjon, entity_t *entity)
{
	if (rpg->index_scene == 0) {
		draw_title_screen(window, &rpg->index_scene);
	}
	else if (rpg->index_scene == 1) {
		draw_menu(window, &rpg->index_scene, rpg->scenes[1]);
	}
	display_game_scene(window, rpg, donjon, entity);
	if (rpg->index_scene == 4) {
		draw_pause_menu(window, &rpg->index_scene, rpg->scenes[4]);
	}
	call_other_scene(window, &rpg->index_scene, rpg);
}
