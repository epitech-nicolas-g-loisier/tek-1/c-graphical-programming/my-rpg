/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** Draw a given room of the dungeon
*/

#include <stdlib.h>
#include "dungeon.h"

int destroy_object(sfTexture *texture, sfRectangleShape *rect)
{
	sfRectangleShape_destroy(rect);
	sfTexture_destroy(texture);
	return (0);
}

static	int draw_map_icon(sfVector2f pos, sfRenderWindow *window, room_t *room)
{
	sfRectangleShape *rectangle = sfRectangleShape_create();
	sfTexture *texture = sfTexture_createFromFile(TEXTURE_ICON, NULL);
	sfIntRect rect = {0, 0, 42, 42};

	if (room->id == MINI_BOSS)
		return (destroy_object(texture, rectangle));
	else if (room->id == MAIN)
		rect.left += 42;
	if (room->id == SHOP)
		rect.left += 84;
	else if (room->id == KEY)
		rect.left += 126;
	pos.x += 16;
	pos.y += 4;
	sfRectangleShape_setPosition(rectangle, pos);
	sfRectangleShape_setSize(rectangle, (sfVector2f){42, 42});
	sfRectangleShape_setTexture(rectangle, texture, sfFalse);
	sfRectangleShape_setTextureRect(rectangle, rect);
	sfRenderWindow_drawRectangleShape(window, rectangle, NULL);
	return (destroy_object(texture, rectangle));
}

static	int draw_room(sfVector2i old_pos, sfRenderWindow *window,
			donjon_t *donjon, sfVector2i idx)
{
	sfVector2u win_size = sfRenderWindow_getSize(window);
	sfRectangleShape *rect = sfRectangleShape_create();
	sfVector2f pos = {(win_size.x - ((donjon->floor - old_pos.x) * 75)), 0};
	sfColor color[2] = {{155, 155, 155, 125}, {255, 255, 255, 155}};

	pos.y = (old_pos.y * 50);
	if (old_pos.x < 0 || old_pos.y >= donjon->floor)
		return (destroy_object(NULL, rect));
	sfRectangleShape_setPosition(rect, pos);
	if (idx.x == donjon->pos.x && idx.y == donjon->pos.y)
		sfRectangleShape_setFillColor(rect, color[1]);
	else
		sfRectangleShape_setFillColor(rect, color[0]);
	sfRectangleShape_setOutlineColor(rect, sfBlack);
	sfRectangleShape_setOutlineThickness(rect, 2);
	sfRectangleShape_setSize(rect, (sfVector2f){73, 48});
	sfRenderWindow_drawRectangleShape(window, rect, NULL);
	if (donjon->rooms[idx.y][idx.x]->id != ROOM)
		draw_map_icon(pos, window, donjon->rooms[idx.y][idx.x]);
	return (destroy_object(NULL, rect));
}

static	void get_offset(room_t ***donjon, int size, sfVector2i *dec)
{
	for (int index = 0; donjon[dec->y][index]->id == VOID; index++) {
		if (index == (size - 1)) {
			dec->y+= 1;
			index = 0;
		}
	}
	for (int index = 0; donjon[index][dec->x]->id == VOID; index++) {
		if (index == (size - 1)) {
			dec->x -= 1;
			index = 0;
		}
	}
}

void	draw_minimap(donjon_t *donjon, sfRenderWindow *win)
{
	room_t ***rooms = donjon->rooms;
	sfVector2i idx = {0, 0};
	sfVector2i dec = {(donjon->floor - 1), 0};
	sfVector2i send = {0, 0};

	get_offset(rooms, donjon->floor, &dec);
	while (rooms[idx.y] != NULL) {
		send.x = idx.x + ((donjon->floor - 1) - dec.x);
		send.y = idx.y - dec.y;
		if (rooms[idx.y][idx.x]->id != VOID &&
			rooms[idx.y][idx.x]->id != BLOCK)
			draw_room(send, win, donjon, idx);
		idx.x++;
		if (rooms[idx.y][idx.x] == NULL) {
			idx.y++;
			idx.x = 0;
		}
	}
}
