/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** draw door on the background
*/

#include "my_rpg.h"

static	void draw_door(sfRenderWindow *window, sfVector2f pos,
			sfVector2f origin, sfIntRect rect)
{
	sfTexture *texture = door_texture(0);
	sfRectangleShape *rectangle = sfRectangleShape_create();
	sfVector2f size = {150, 150};
	sfVector2f scale = {1, 1};

	if (pos.x == WIDTH) {
		scale.x = -1;
		origin.x = origin.x * -1;
	} else if (pos.y == HEIGHT) {
		scale.y = -1;
		origin.y = origin.y * -1;
	}
	sfRectangleShape_setScale(rectangle, scale);
	sfRectangleShape_setOrigin(rectangle, origin);
	sfRectangleShape_setTexture(rectangle, texture, sfFalse);
	sfRectangleShape_setTextureRect(rectangle, rect);
	sfRectangleShape_setPosition(rectangle, pos);
	sfRectangleShape_setSize(rectangle, size);
	sfRenderWindow_drawRectangleShape(window, rectangle, NULL);
	sfRectangleShape_destroy(rectangle);
}

char draw_up_door(sfRenderWindow *window, room_t ***donjon, sfVector2i idx,
			sfVector2f origin)
{
	sfIntRect rect = {0, 0, 150, 150};
	sfVector2f pos = {(X_MID) - 75, 0};

	if (idx.y < 1)
		return (0);
	else if (donjon[idx.y][idx.x]->close == false) {
			rect.top += 450;
	}
	if (donjon[idx.y - 1][idx.x]->id == VOID) {
		return (0);
	} else if (donjon[idx.y - 1][idx.x]->id == BLOCK) {
		return (0);
	}
	draw_door(window, pos, origin, rect);
	return (0);
}

char draw_right_door(sfRenderWindow *window, room_t ***donjon,
			sfVector2i idx, sfVector2f origin)
{
	sfIntRect rect = {150, 0, 150, 150};
	sfVector2f pos = {(WIDTH), (Y_MID) - 75};

	if (donjon[idx.y][idx.x + 1] == NULL)
		return (0);
	else if (donjon[idx.y][idx.x]->close == false) {
		rect.top += 450;
	}
	if (donjon[idx.y][idx.x + 1]->id == VOID) {
		return (0);
	} else if (donjon[idx.y][idx.x + 1]->id == BLOCK) {
		return (0);
	}
	draw_door(window, pos, origin, rect);
	return (0);
}

char draw_down_door(sfRenderWindow *window, room_t ***donjon,
			sfVector2i idx, sfVector2f origin)
{
	sfVector2f pos = {(X_MID) - 75, (HEIGHT)};
	sfIntRect rect = {0, 0, 150, 150};

	if (donjon[idx.y + 1] == NULL)
		return (0);
	else if (donjon[idx.y][idx.x]->close == false) {
		rect.top += 450;
	}
	if (donjon[idx.y + 1][idx.x]->id == VOID) {
		return (0);
	} else if (donjon[idx.y + 1][idx.x]->id == BLOCK) {
		return (0);
	}
	draw_door(window, pos, origin, rect);
	return (0);
}

char draw_left_door(sfRenderWindow *window, room_t ***donjon,
			sfVector2i idx, sfVector2f origin)
{
	sfVector2f pos = {0, (Y_MID) - 75};
	sfIntRect rect = {150, 0, 150, 150};

	if (idx.x < 1)
		return (0);
	else if (donjon[idx.y][idx.x]->close == false) {
		rect.top += 450;
	}
	if (donjon[idx.y][idx.x - 1]->id == VOID) {
		return (0);
	} else if (donjon[idx.y][idx.x - 1]->id == BLOCK) {
		return (0);
	}
	draw_door(window, pos, origin, rect);
	return (0);
}
