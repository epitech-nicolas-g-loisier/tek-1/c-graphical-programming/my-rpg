/*
** EPITECH PROJECT, 2018
** My RPG
** File description:
** open window
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_rpg.h"
#include "dungeon.h"
#include "entity.h"
#include "scene.h"

static	sfRenderWindow *my_window_create(sfRenderWindow *window)
{
	sfVideoMode mode;

	mode.width = WIDTH;
	mode.height = HEIGHT;
	mode.bitsPerPixel = 32;
	window = sfRenderWindow_create(mode, "My RPG", sfFullscreen, NULL);
	sfRenderWindow_setFramerateLimit(window, 30);
	sfRenderWindow_setVerticalSyncEnabled(window, sfTrue);
	return (window);
}

static int	destroy_data(sfRenderWindow *window, donjon_t *donjon)
{
	sfRenderWindow_destroy(window);
	play_music(-1);
	door_texture(1);
	free_donjon(donjon);
	get_button_texture(-1);
	return (0);
}

static void	init_entity_list(entity_t **entity, texture_list_t *texture)
{
	sfVector2f pos = {200, 200};

	texture->img = sfTexture_createFromFile("assets/heart.png", NULL);
	texture->rect = (sfIntRect){0, 0, 17, 16};
	texture->next->img = sfTexture_createFromFile("assets/link.png", NULL);
	texture->next->rect = (sfIntRect){0, 0, 24, 25};
	*entity = create_entity(PLAYER, pos, texture->next,
	(sfVector2f){3.8, 3.8});
	pos.x = 500;
	(*entity)->next = create_entity(MOB, pos, texture,
	(sfVector2f){3, 3});
	pos.y = 400;
	(*entity)->next->next = create_entity(OBS, pos, texture,
	(sfVector2f){3, 3});
}
static	void display_intro(sfRenderWindow *window)
{
	display_pegi(window);
	play_music(0);
	display_title_screen_anim(window);
}

int	open_window(void)
{
	int size = 9;
	sfRenderWindow *window = NULL;
	entity_t *entity_list = NULL;
	texture_list_t texture;
	texture_list_t scnd_texture;
	donjon_t *donjon = init_donjon(size);
	rpg_t *rpg = malloc(sizeof(rpg_t));

	texture.next = &scnd_texture;
	init_entity_list(&entity_list, &texture);
	window = my_window_create(window);
	rpg->scenes = create_scenes();
	rpg->index_scene = 0;
	display_intro(window);
	while (sfRenderWindow_isOpen(window)) {
		manage_keyboard(entity_list, &rpg->index_scene);
		display_scene(window, rpg, donjon, entity_list);
		sfRenderWindow_display(window);
	}
	destroy_data(window, donjon);
	return (0);
}
